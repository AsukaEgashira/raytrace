//
//  kdtreeBase.hpp
//  rending
//
//  Created by togashi on 2017/09/06.
//  Copyright © 2017年 togashi. All rights reserved.
//

#ifndef kdtreeBase_hpp
#define kdtreeBase_hpp

//#include <stdio.h>
//#include "Accel.h"
//
//void hello();
//
//struct Node
//{
//public:
//    Node *parent;
//    Node *left;//left,right mean children
//    Node *right;
//    TriangleMesh *core;
//    
//    bool isRoot=false;
//    
//    double pre_space_size = -100;
//    double xmin = pre_space_size;
//    double xmax = pre_space_size;
//    double ymin = pre_space_size;
//    double ymax = pre_space_size;
//    double zmin = pre_space_size;
//    double zmax = pre_space_size;
//    std::vector<TriangleMesh*> belonging_particles;
//    std::vector<TriangleMesh*> list;
//    //Node(bool isRoot):isRoot(isRoot),core(t_particle(PVector(-10,-10), PVector(0,0), -1,-1)){};
////    
////    Node(t_particle core,bool isRoot=false):isRoot(isRoot),core(core){}
////    Node(t_particle core, double xmin, double xmax,double ymin, double ymax,bool isRoot=false):isRoot(isRoot),core(core),xmin(xmin),xmax(xmax),ymin(ymax),ymax(ymin){}
////    
////    Node( double xmin, double xmax,double ymin, double ymax,bool isRoot=false):isRoot(isRoot),core(t_particle(PVector(-10,-10), PVector(0,0), -1,-1)),xmin(xmin),xmax(xmax),ymin(ymin),ymax(ymax){}
////    
////    Node():isRoot(false),core(t_particle(PVector(-10,-10), PVector(0,0), -1,-1)){};
//};
//
//
//
//
//class axisSorter {
//    int k;//k = 0 or 1  0:x座標で分割 1:y座標で分割 2:zで分割
//public:
//    axisSorter(int _k) : k(_k) {}
//    double operator()(const TriangleMesh &a, const TriangleMesh &b)//; 外す
//    {
//        //return a. < particle_b.position.pvec[k];
//        return true;
//    }
//};
//
//
//inline bool isInside(const Node &node,const Triangle &ball)
//{
//    if(node.isRoot){return true;}//true登録まだ
//    
//    bool ans=false;
//
////    //std::cout << "after isRoot  index" << ball.index << std::endl;
//    
////    if((node.xmin < (ball.position.pvec[0] -ball.radius)) &&  ((ball.position.pvec[0] + ball.radius) < node.xmax))
////    {
////        if((node.ymin < (ball.position.pvec[1] -ball.radius)) &&  ((ball.position.pvec[1] + ball.radius) < node.ymax))
////        {
////            ans = true;
////        }
////    }
////    
//    return ans;
//}
//
////Node *makeKDTree(Objects &target, int l, int r, int depth,float xmin,float xmax,float ymin,float ymax,float zmin,float zmax,Object* Core,int direction);
//
////inline void registerParticleToNode(Node *node,t_particle &particle)
////{
////    
////    if(node->core.index == particle.index)
////    {
////        node->belonging_particles.push_back(&particle);
////        return;
////    }
////    
////    //std::cout << "check particles  index" << particle.index << std::endl;
////    
////    //bool inside = true;
////    
////    bool inside_left=false;
////    bool inside_right=false;
////    
////    if(particle.index == 0){
////        std::cout <<"";
////    }
////    
////    if(particle.index==-1){
////        
////        std::cout<<"";
////    }
////    
////    
////    if(node->right ){
////        inside_right=isInside(*(node->right),particle);
////    }
////    
////    if(node->left ){
////        inside_left=isInside(*(node->left),particle);
////    }
////    
////    //if(inside_left){node=node->left;break;}
////    if(inside_left == true){registerParticleToNode(node->left,particle);}
////    //if(inside_right){node=node->right;break;}
////    
////    if(inside_right == true){registerParticleToNode(node->right, particle);}
////    
////    if(inside_left==false && inside_right==false)
////    {
////        node->belonging_particles.push_back(&particle);
////        //inside = false;
////        return;
////    }
////    
////}
//
//
//void real_collision(Node *node);
//void real_check_collision(Node* node);
//void real_make_candidate(Node *node);
//void delete_root(Node *node);
//
////inline void kdtree(std::vector<t_particle> &target_particles)
////{
////    //Node *root;
////    Node *root = new Node;
////    
////    root = makeKDTree(target_particles, 0, target_particles.size(), 0, left_, right_, ceil_, floor_, nullptr, -1);
////    root->isRoot=true;
////    
////    //Nodeにballを登録 ballを順に
////    for (int i = 0; i < target_particles.size(); i++)
////    {
////        registerParticleToNode(root,target_particles[i]);
////    }
////    
////    real_collision(root);
////    
////    delete_root(root);
////    
////}
#endif /* kdtreeBase_hpp */
