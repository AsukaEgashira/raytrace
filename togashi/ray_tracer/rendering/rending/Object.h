#ifndef MIRO_OBJECT_H_INCLUDED
#define MIRO_OBJECT_H_INCLUDED

#include <vector>
#include "Miro.h"
#include "Material.h"

#include "Node.hpp"

class Object
{
public:
    Object() {}
    virtual ~Object() {}
    
    void setMaterial(const Material* m) {m_material = m;}
    
    virtual void renderGL() {}
    virtual void preCalc() {}
    
    virtual bool intersect(HitInfo& result, const Ray& ray, float tMin = 0.0f, float tMax = MIRO_TMAX) = 0;
    
    virtual void belong(Node* root){};
    
    //virtual float gravity_center(int k);

    
protected:
    const Material* m_material;
};

typedef std::vector<Object*> Objects;

#endif // MIRO_OBJECT_H_INCLUDED
