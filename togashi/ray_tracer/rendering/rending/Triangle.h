#ifndef MIRO_TRIANGLE_H_INCLUDED
#define MIRO_TRIANGLE_H_INCLUDED

#include "Object.h"
#include "kdtreeBase.hpp"

#include "global_variable.hpp"

/*
	The Triangle class stores a pointer to a mesh and an index into its
	triangle array. The mesh stores all data needed by this Triangle.
*/
class Triangle : public Object
{
public:
    Triangle(TriangleMesh * m = 0, unsigned int i = 0);
    virtual ~Triangle();
    
    void setIndex(unsigned int i) {m_index = i;}
    void setMesh(TriangleMesh* m) {m_mesh = m;}
    
    virtual void renderGL();
    virtual bool intersect(HitInfo& result, const Ray& ray, float tMin = 0.0f, float tMax = MIRO_TMAX);
    
    //original
    int bitsep(int b_s, int dim);
    int calcurate_morton(int x, int y, int z);
    //using Object::belong;
    virtual void belong(Node* root);
    
    //kd tree
    float gravity_center(int k);
    int kd_return_index(){return m_index;};
    float kd_return_max(int k);
    float kd_return_min(int k);
    
protected:
    TriangleMesh* m_mesh;
    unsigned int m_index;
};

#endif // MIRO_TRIANGLE_H_INCLUDED
