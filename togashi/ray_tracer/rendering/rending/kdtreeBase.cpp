//
//  kdtreeBase.cpp
//  rending
//
//  Created by togashi on 2017/09/06.
//  Copyright © 2017年 togashi. All rights reserved.
//

#include "kdtreeBase.hpp"


//void hello()
//{
//   std::cout<<"Hello------"<<std::endl;
//}
//
//Node *makeKDTree(Objects &target, int l, int r, int depth,float xmin,float xmax,float ymin,float ymax,float zmin,float zmax,Object* Core,int direction)
//{   
//    //directionの使い方
//    //0 : x方向left
//    //1 : x方向right
//    //2 : y方向left
//    //3 : y方向right
//    if (l >= r)return NULL;
//    
//    sort(target_particles.begin() + l, target_particles.begin() + r, axisSorter(depth % 2));
//    int mid = (l+r)>>1;
//    
//    Node *node = new Node();
//    node->core =  target_particles[mid];
//    
//    switch (direction)
//    {
//        case 0:
//            node->xmin = xmin;
//            node->xmax = parent_core->position[0];
//            node->ymin = ymin;
//            node->ymax = ymax;
//            node->isRoot=false;
//            break;
//        case 1:
//            node->xmin = parent_core->position[0];
//            node->xmax = xmax;
//            node->ymin = ymin;
//            node->ymax = ymax;
//            node->isRoot=false;
//            break;
//        case 2:
//            node->xmin = xmin;
//            node->xmax = xmax;
//            node->ymin = ymin;
//            node->ymax = parent_core->position[1];
//            node->isRoot=false;
//            
//            break;
//        case 3:
//            node->xmin = xmin;
//            node->xmax = xmax;
//            node->ymin = parent_core->position[1];
//            node->ymax = ymax;
//            node->isRoot=false;
//            break;
//        default:
//            break;
//    }
//    
//    if(depth % 2 == 0)//x方向で分ける
//    {
//        node->left = makeKDTree(target_particles, l, mid, depth+1,node->xmin,node->xmax,node->ymin,node->ymax,&node->core,0);//再帰関数
//        if(node->left)node->left->parent=node;//if(v->left) means if(v->left != nullptr)
//        
//        node->right = makeKDTree(target_particles, mid+1, r, depth+1,node->xmin,node->xmax,node->ymin,node->ymax,&node->core,1);//再帰関数
//        if(node->right){
//            node->right->parent=node;
//            //std::cout << "****** 2 hi! **********" << std::endl;
//        }
//    }
//    
//    if(depth %2 == 1)//y方向で分ける
//    {
//        node->left = makeKDTree(target_particles, l, mid, depth+1,node->xmin,node->xmax,node->ymin,node->ymax,&node->core,2);//再帰関数
//        if(node->left)node->left->parent=node;//if(v->left) means if(v->left != nullptr)
//        
//        node->right = makeKDTree(target_particles, mid+1, r, depth+1,node->xmin,node->xmax,node->ymin,node->ymax,&node->core,3);//再帰関数
//        if(node->right){
//            node->right->parent=node;
//            //std::cout << "****** 1 hi! **********" << std::endl;
//        }
//    }
//    
//    return node;
//}

//void real_make_candidate(Node *node)
//{
//    dcout("make_candidate");
//    dcout("\n");
//    
//    for(int i=0;i < node->belonging_particles.size();i++)
//    {
//        node->list.push_back(node->belonging_particles[i]);
//    }
//    
//    dcout("make_candidate　2");
//    dcout("\n");
//    
//    dcout(node->isRoot);;
//    dcout("\n");
//    
//    
//    if(!node->isRoot)
//    {
//        for(int i=0;i < node->parent->list.size();i++)
//        {
//            node->list.push_back(node->parent->list[i]);
//        }
//    }
//    dcout("make_candidate　3");
//    dcout("\n");
//    
//    if(node->left){real_make_candidate(node->left);}
//    if(node->right){real_make_candidate(node->right);}
//}
//
//void real_check_collision(Node* node)
//{
//    for(int i=0;i < node->belonging_particles.size();i++)
//    {
//        dcout("belonging particle index i =");
//        dcout(node->belonging_particles[i]->index);
//        dcout("-------------");
//        dcout("\n");
//        
//        for(int j=i+1;j < node->list.size();j++)
//        {
//            node->belonging_particles[i]->collide(*(node->list[j]));
//            //            std::cout << "list particle index" << node->list[j]->index<<std::endl;
//            
//            dcout( "list particle index");
//            dcout(node->list[j]->index);
//            dcout("\n");
//        }
//        node->belonging_particles.clear();
//    }
//    if(node->left){real_check_collision(node->left);}
//    if(node->right){real_check_collision(node->right);}
//}
//
//void delete_root(Node *node)
//{
//    //    node->belonging_particles.clear();
//    node->list.clear();
//    if(node->left){delete_root(node->left);}
//    if(node->right){delete_root(node->right);}
//    
//    delete node;
//}
//
//void real_collision(Node *node)
//{
//    real_make_candidate(node);
//    real_check_collision(node);
//    
//}


