#include "Accel.h"
#include "Ray.h"
#include "Console.h"
//#include "Node.hpp"

#include <algorithm>

//#include <Eigen/Dense>
#include "/Users/togashi/raytrace/togashi/ray_tracer/Eigen/Dense"
//#include <igl/readOBJ.h>
#include </Users/togashi/raytrace/togashi/ray_tracer/igl/readOBJ.h>

#include "Triangle.h"
#include "TriangleMesh.h"
#include "global_variable.hpp"
#include <string>

Eigen::MatrixXd V;
Eigen::MatrixXi F;

void
Accel::build(Objects * objs)
{
    // construct the bounding volume hierarchy
    m_objects = objs;
    
    switch (TreeType) {
        case 0:
            break;
        case 1://Out Tree
            OctBuild_tree();
            OctRegister_tri();
            break;
            
        case 2:
            kd_build();//make tree and candidatable list
            break;
            
        default:
            std::cout << "----  Not exist Tree Type ----" << std::endl;
            break;
    }
}

void
Accel::test(){
    Ray test_ray = Ray({10.0,10.0,10.0}, {-0.5518, -0.4904, -0.6744});
    bool test_aabb=OctRay_aabb_intersect(root->children[5], test_ray);
    std::cout<<"can we detect ray and aabb collision? : "<<test_aabb<<std::endl;
    std::cout<<"";
    
    std::cout<<"count triagnle num"<<std::endl<<root->ob_index.size()<<", ";
    for(int i=0;i<8;i++){
        std::cout<<root->children[i]->ob_index.size()<<", ";
    }
    std::cout<<"";
    std::cout<<"";
}

vector<float>
Accel::OctIntersectTime(float degm, float degM, const Ray& ray, int ver) const{
    float t_m=0.0, t_M=0.0;
    if(ver==0){
        if(ray.d.x > 0.0){
            t_m = (degm - ray.o.x) / ray.d.x;
            t_M = (degM - ray.o.x) / ray.d.x;
        }
        else if(ray.d.x < 0.0){
            t_M = (degm - ray.o.x) / ray.d.x;
            t_m = (degM - ray.o.x) / ray.d.x;
        }
        else{
            t_m = -100000.0;
            t_M = 100000.0;
        }
        return {t_m, t_M};
    }
    else if(ver==1){
        if(ray.d.y > 0.0){
            t_m = (degm - ray.o.y) / ray.d.y;
            t_M = (degM - ray.o.y) / ray.d.y;
        }
        else if(ray.d.y < 0.0){
            t_M = (degm - ray.o.y) / ray.d.y;
            t_m = (degM - ray.o.y) / ray.d.y;
        }
        else{
            t_m = -100000.0;
            t_M = 100000.0;
        }
        return {t_m, t_M};
    }
    else{
        if(ray.d.z > 0.0){
            t_m = (degm - ray.o.z) / ray.d.z;
            t_M = (degM - ray.o.z) / ray.d.z;
        }
        else if(ray.d.z < 0.0){
            t_M = (degm - ray.o.z) / ray.d.z;
            t_m = (degM - ray.o.z) / ray.d.z;
        }
        else{
            t_m = -100000;
            t_M = 100000;
        }
        return {t_m, t_M};
    }
}

bool
Accel::OctRay_aabb_intersect(Node* node, const Ray& ray) const{
    float tm;
    float tM;
    
    vector<float> tx = OctIntersectTime(node->left, node->right, ray, 0);
    vector<float> ty = OctIntersectTime(node->floor, node->ceiling, ray, 1);
    vector<float> tz = OctIntersectTime(node->front, node->back, ray, 2);
    
    tm = std::max({tx[0], ty[0], tz[0]});
    tM = std::min({tx[1], ty[1], tz[1]});
    
    //std::cout<<"tM - tm = "<<tM - tm<<std::endl;
    if(tM-tm>=0){
        return true;
    }
    return false;
}

bool
Accel::OctLoop_node(HitInfo& minHit, float* tMin, float* tMax, Node* node, const Ray& ray) const{
    bool hit;
    HitInfo tempMinHit;
    if(!node->parent){
        hit = false;
        
        //floor
        if((*m_objects)[m_objects->size()-1]->intersect(tempMinHit, ray)){
            hit = true;
            if (tempMinHit.t < minHit.t){
                minHit = tempMinHit;
            }
        }
    }
    
    bool intersect = OctRay_aabb_intersect(node, ray);
    
    if(intersect){
        
        //if ray intersect triangle
        //for loop about belong of the node
        
        for(size_t i=0;i<node->ob_index.size();i++){
            if ((*m_objects)[node->ob_index[i]]->intersect(tempMinHit, ray, *tMin, *tMax))
            {
                hit = true;
                if (tempMinHit.t < minHit.t){
                    minHit = tempMinHit;
                }
            }
        }
    }
    
    if(node->children[0]&&intersect){
        for(int i=0;i<8;i++){
            hit = hit|OctLoop_node(minHit, tMin, tMax, node->children[i], ray);
        }
    }
    
    return hit;
}

bool
Accel::intersect(HitInfo& minHit, const Ray& ray, float tMin, float tMax) const
{
    // Here you would need to traverse the acceleration data structure to perform ray-intersection
    // acceleration. For now we just intersect every object.
    
    bool hit = false;
    HitInfo tempMinHit;
    minHit.t = MIRO_TMAX;
    
//    if(tree){
//        hit = OctLoop_node(minHit, &tMin, &tMax, root, ray);
//    }
//    else{
//        for (size_t i = 0; i < m_objects->size(); ++i)
//        {
//            if ((*m_objects)[i]->intersect(tempMinHit, ray, tMin, tMax))
//            {
//                hit = true;
//                if (tempMinHit.t < minHit.t)
//                    minHit = tempMinHit;
//            }
//        }
//        std::cout << std::endl;
//    }
    
    switch (TreeType) {
        case 0://全探索
            for (size_t i = 0; i < m_objects->size(); ++i)
            {
                if ((*m_objects)[i]->intersect(tempMinHit, ray, tMin, tMax))
                {
                    hit = true;
                    if (tempMinHit.t < minHit.t)
                        minHit = tempMinHit;
                }
            }
            std::cout << std::endl;
            break;
        case 1://Oct kree
            hit = OctLoop_node(minHit, &tMin, &tMax, root, ray);
            break;
        case 2://kd tree
            hit = kd_Loop_node(minHit, &tMin, &tMax,kd_root,ray);
        default:
            break;
    }
    
    return hit;
}

void
Accel::OctDevideSpatial(Node* node, float ceil, float floor, float left, float right, float back, float front, int depth){
    if(depth > depth_t){
        for(int i=0; i<8; i++){
            node->children[i] = nullptr;
        }
        return;
    }
    
    float mid_h = (ceil + floor)/2, mid_w = (right + left)/2, mid_d = (back + front)/2;
    
    float x_max[8] = {mid_w, right, mid_w, right, mid_w, right, mid_w, right};
    float x_min[8] = {left, mid_w, left, mid_w, left, mid_w, left, mid_w};
    float y_max[8] = {mid_h, mid_h, ceil, ceil, mid_h, mid_h, ceil, ceil};
    float y_min[8] = {floor, floor, mid_h, mid_h, floor, floor, mid_h, mid_h};
    float z_max[8] = {mid_d, mid_d, mid_d, mid_d, back, back, back, back};
    float z_min[8] = {front, front, front, front, mid_d, mid_d, mid_d, mid_d};
    
    for(int i=0;i<8;i++){
        node->children[i] = new Node(y_max[i], y_min[i], x_min[i], x_max[i], z_max[i], z_min[i], depth);
        node->children[i]->parent = node;
        
        //        std::cout<<"children " << i << " : "<<std::endl;
        //        std::cout<<node->children[i]->left<<", "<<node->children[i]->right<<", "<<node->children[i]->floor<<", "
        //        <<node->children[i]->ceiling<<", "<<node->children[i]->front<<", "<<node->children[i]->back<<std::endl;
        
        int c_dep = depth + 1;
        OctDevideSpatial(node->children[i], y_max[i], y_min[i], x_min[i], x_max[i], z_max[i], z_min[i], c_dep);
    }
}

void
Accel::OctBuild_tree(){
    //get max and min of width, height, depth
    if(m_objects->size()<600){
        igl::readOBJ("/Users/togashi/raytrace/togashi/ray_tracer/data" "/teapot.obj", V, F);
    }
    else if (m_objects->size()<70000){
        igl::readOBJ("/Users/togashi/raytrace/togashi/ray_tracer/data" "/bunny.obj", V, F);
    }
    else{
        igl::readOBJ("/Users/togashi/raytrace/togashi/ray_tracer/data" "/sponza.obj", V, F);
    }

    std::cout << "Can read? " << std::endl;
    std::cout << "Can read? " << std::endl;
    
    Eigen::Vector3d m = V.colwise().minCoeff();
    Eigen::Vector3d M = V.colwise().maxCoeff();
    
    left_ = m(0);
    right_ = M(0);
    floor_ = m(1);
    ceil_ = M(1);
    front_ = m(2);
    back_ = M(2);
    
    dx_ = (right_ - left_) / std::pow(2.0, depth_t);
    dy_ = (ceil_ - floor_) / std::pow(2.0, depth_t);
    dz_ = (back_ - front_) / std::pow(2.0, depth_t);
    
    root = new Node(ceil_, floor_, left_, right_, back_, front_, 0);
    root->parent = nullptr;
    
    std::cout<<"root :"<<std::endl;
    std::cout<<root->left<<", "<<root->right<<", "<<root->floor<<", "
    <<root->ceiling<<", "<<root->front<<", "<<root->back<<std::endl;
    
    OctDevideSpatial(root, ceil_, floor_, left_, right_, back_, front_, 1);
    
    //    std::cout<<"**************"<<std::endl;
    //    std::cout<<"devide spatial"<<std::endl;
    //    std::cout<<"**************"<<std::endl;
}

void
Accel::OctRegister_tri(){
    for (size_t i = 0; i < m_objects->size() - 1; ++i)
    {
        (*m_objects)[i]->belong(root);
    }
}

//------------ kd function -------------------------------------

void kd_make_candidateList(kd_Node *kd_node)
{
    for(int i=0;i < kd_node->belonging_Triangles.size();i++)
    {
        kd_node->list.push_back(kd_node->belonging_Triangles[i]);
    }
    
    if(!kd_node->isRoot)
    {
        for(int i=0;i < kd_node->parent->list.size();i++)
        {
            kd_node->list.push_back(kd_node->parent->list[i]);
        }
    }
    
    if(kd_node->left){kd_make_candidateList(kd_node->left);}
    if(kd_node->right){kd_make_candidateList(kd_node->right);}
}

void Accel::kd_build()
{
    if(m_objects->size()<600){
        igl::readOBJ("/Users/togashi/raytrace/togashi/ray_tracer/data" "/teapot.obj", V, F);
    }
    else if (m_objects->size()<70000){
        igl::readOBJ("/Users/togashi/raytrace/togashi/ray_tracer/data" "/bunny.obj", V, F);
    }
    else{
        igl::readOBJ("/Users/togashi/raytrace/togashi/ray_tracer/data" "/sponza.obj", V, F);
    }
    
    //std::cout << "Can read? " << std::endl;
    //std::cout << "Can read? " << std::endl;
    
    Eigen::Vector3d m = V.colwise().minCoeff();
    Eigen::Vector3d M = V.colwise().maxCoeff();
    
    left_ = m(0);//xmin
    right_ = M(0);//xmax
    floor_ = m(1);//ymin
    ceil_ = M(1);//ymax
    front_ = m(2);//zmin
    back_ = M(2);//zmax
    
    //make root
    //kd_Node* kd_root;
    
    kd_root = kd_maketree(m_objects,0, m_objects->size(), 0, left_, right_, floor_, ceil_, front_, back_, nullptr, -1);
    kd_root->isRoot=true;
    
    
    //register triangle to node
    for(int i=0 ;i <m_objects->size() ;i++)
    {
        Triangle* tri_obj = dynamic_cast<Triangle*>((*m_objects)[i]);
        registerTriangleToNode(kd_root,*tri_obj);
    }
    
    //make list : list mean my belonging triangles and parent's belonging triangles
    kd_make_candidateList(kd_root);
    
}

kd_Node* Accel::kd_maketree(Objects* kd_objects,int l,int r,int depth,float xmin,float xmax,float ymin,float ymax,float zmin,float zmax,Object *parent_core,int direction)
{
    //directionの使い方
    //0 : x方向left
    //1 : x方向right
    //2 : y方向left
    //3 : y方向right
    //4 : z方向left
    //5 : z方right
    
    std::cout << "start l,r =" << l <<"," << r <<std::endl;
    if (l >= r)return NULL;

    //sort(target_particles.begin() + l, target_particles.begin() + r, axisSorter(depth % 2));
    //sort(m_objects[l] , m_objects[r], axisSorter(depth % 3));
    
    sort(m_objects->begin()+l , m_objects->begin()+r, axisSorter(depth % 3));

    int mid = (l+r)>>1;
    
    std::cout << "mid =" << mid
    << "  depth =" << depth<< std::endl;
    
    kd_Node* kd_node = new kd_Node();
    
    kd_node->core = (*m_objects)[mid];
   
    
    Triangle* tri_parent_core = dynamic_cast<Triangle*>(parent_core);
    switch (direction) {
        case 0:

            kd_node->xmin = xmin;
            kd_node->xmax = tri_parent_core->gravity_center(0);
            kd_node->ymin = ymin;
            kd_node->ymax = ymax;
            kd_node->zmin = zmin;
            kd_node->zmax = zmax;
            kd_node->isRoot=false;
            break;
        case 1:
            kd_node->xmin = tri_parent_core->gravity_center(0);
            kd_node->xmax = xmax;
            kd_node->ymin = ymin;
            kd_node->ymax = ymax;
            kd_node->zmin = zmin;
            kd_node->zmax = zmax;
            kd_node->isRoot=false;
            break;
        case 2:
            kd_node->xmin = xmin;
            kd_node->xmax = xmax;
            kd_node->ymin = ymin;
            kd_node->ymax = tri_parent_core->gravity_center(1);
            kd_node->zmin = zmin;
            kd_node->zmax = zmax;
            kd_node->isRoot=false;
            break;
        case 3:
            kd_node->xmin = xmin;
            kd_node->xmax = xmax;
            kd_node->ymin = tri_parent_core->gravity_center(1);
            kd_node->ymax = ymax;
            kd_node->zmin = zmin;
            kd_node->zmax = zmax;
            kd_node->isRoot=false;
            break;
        case 4:
            kd_node->xmin = xmin;
            kd_node->xmax = xmax;
            kd_node->ymin = ymin;
            kd_node->ymax = ymax;
            kd_node->zmin = zmin;
            kd_node->zmax = tri_parent_core->gravity_center(2);
            kd_node->isRoot=false;
            break;
        case 5:
            kd_node->xmin = xmin;
            kd_node->xmax = xmax;
            kd_node->ymin = ymin;
            kd_node->ymax = ymax;
            kd_node->zmin = tri_parent_core->gravity_center(2);
            kd_node->zmax = zmax;
            kd_node->isRoot=false;
            break;
        case -1:
            kd_node->xmin = xmin;
            kd_node->xmax = xmax;
            kd_node->ymin = ymin;
            kd_node->ymax = ymax;
            kd_node->zmin = zmin;
            kd_node->zmax = zmax;
            kd_node->isRoot=true;
            break;
            
    
        default:
            std::cerr<<"error in kd-tree construction";
            exit(0);
            break;
    }
    
    if(depth % 3 == 0)//x方向で分ける
    {
        kd_node->left = kd_maketree(kd_objects,l,mid,depth+1,kd_node->xmin,kd_node->xmax,kd_node->ymin,kd_node->ymax,kd_node->zmin,kd_node->zmax,kd_node->core,0);//再帰関数
        if(kd_node->left){kd_node->left->parent=kd_node;}
        
        kd_node->right = kd_maketree(kd_objects,mid+1,r,depth+1,kd_node->xmin,kd_node->xmax,kd_node->ymin,kd_node->ymax,kd_node->zmin,kd_node->zmax,kd_node->core,1);//再帰関数
        if(kd_node->right){kd_node->right->parent=kd_node;}
    }

    if(depth %3 == 1)//y方向で分ける
    {
        kd_node->left = kd_maketree(kd_objects,l,mid,depth+1,kd_node->xmin,kd_node->xmax,kd_node->ymin,kd_node->ymax,kd_node->zmin,kd_node->zmax,kd_node->core,2);//再帰関数
        if(kd_node->left){kd_node->left->parent=kd_node;}
        
        kd_node->right = kd_maketree(kd_objects,mid+1,r,depth+1,kd_node->xmin,kd_node->xmax,kd_node->ymin,kd_node->ymax,kd_node->zmin,kd_node->zmax,kd_node->core,3);//再帰関数
        if(kd_node->right){kd_node->right->parent=kd_node;}
    }
    
    if(depth %3 == 2)//z方向で分ける
    {
        kd_node->left = kd_maketree(kd_objects,l,mid,depth+1,kd_node->xmin,kd_node->xmax,kd_node->ymin,kd_node->ymax,kd_node->zmin,kd_node->zmax,kd_node->core,4);//再帰関数
        if(kd_node->left){kd_node->left->parent=kd_node;}
        
        kd_node->right = kd_maketree(kd_objects,mid+1,r,depth+1,kd_node->xmin,kd_node->xmax,kd_node->ymin,kd_node->ymax,kd_node->zmin,kd_node->zmax,kd_node->core,5);//再帰関数
        if(kd_node->right){kd_node->right->parent=kd_node;}
    }
    
    return kd_node;
}

bool Accel::kd_isInside(kd_Node &kd_node,Triangle &tri_obj)
{
    if(kd_node.isRoot){return true;}
    
    bool ans=false;
   
    //std::cout << "after isRoot  index" << ball.index << std::endl;
    
    if((kd_node.xmin <  tri_obj.kd_return_min(0)) &&  (tri_obj.kd_return_max(0) < kd_node.xmax))
    {
        if((kd_node.ymin <  tri_obj.kd_return_min(1)) &&  (tri_obj.kd_return_max(1) < kd_node.ymax))
        {
            if((kd_node.zmin <  tri_obj.kd_return_min(2)) &&  (tri_obj.kd_return_max(2) < kd_node.zmax))
            {
                ans = true;
            }

        }
    }
    return ans;
}

void Accel::registerTriangleToNode(kd_Node* kd_node,Triangle &tri_obj)
{
    Triangle* node_tri_core = dynamic_cast<Triangle*>(kd_node->core);
        if(node_tri_core->kd_return_index() == tri_obj.kd_return_index())
        {
            kd_node->belonging_Triangles.push_back(&tri_obj);
            return;
        }
        
        bool inside_left=false;
        bool inside_right=false;
        
        if(kd_node->right){
            inside_right=kd_isInside(*(kd_node->right),tri_obj);
        }
        
        if(kd_node->left){
            inside_left=kd_isInside(*(kd_node->left),tri_obj);
        }
        
        if(inside_left == true){registerTriangleToNode(kd_node->left,tri_obj);}
        
        if(inside_right == true){registerTriangleToNode(kd_node->right, tri_obj);}
        
        if(inside_left==false && inside_right==false)
        {
            kd_node->belonging_Triangles.push_back(&tri_obj);
            return;
        }
}

vector<float>
Accel::kd_IntersectTime(float degm, float degM, const Ray& ray, int ver) const{
    float t_m=0.0, t_M=0.0;
    if(ver==0){
        if(ray.d.x > 0.0){
            t_m = (degm - ray.o.x) / ray.d.x;
            t_M = (degM - ray.o.x) / ray.d.x;
        }
        else if(ray.d.x < 0.0){
            t_M = (degm - ray.o.x) / ray.d.x;
            t_m = (degM - ray.o.x) / ray.d.x;
        }
        else{
            t_m = -100000.0;
            t_M = 100000.0;
        }
        return {t_m, t_M};
    }
    else if(ver==1){
        if(ray.d.y > 0.0){
            t_m = (degm - ray.o.y) / ray.d.y;
            t_M = (degM - ray.o.y) / ray.d.y;
        }
        else if(ray.d.y < 0.0){
            t_M = (degm - ray.o.y) / ray.d.y;
            t_m = (degM - ray.o.y) / ray.d.y;
        }
        else{
            t_m = -100000.0;
            t_M = 100000.0;
        }
        return {t_m, t_M};
    }
    else{
        if(ray.d.z > 0.0){
            t_m = (degm - ray.o.z) / ray.d.z;
            t_M = (degM - ray.o.z) / ray.d.z;
        }
        else if(ray.d.z < 0.0){
            t_M = (degm - ray.o.z) / ray.d.z;
            t_m = (degM - ray.o.z) / ray.d.z;
        }
        else{
            t_m = -100000;
            t_M = 100000;
        }
        return {t_m, t_M};
    }
}

bool Accel::kd_intersect_with_node(kd_Node* kd_node,const Ray& ray)const
{
    float tmin;
    float tMax;
    
    vector<float> tx = kd_IntersectTime(kd_node->xmin, kd_node->xmax, ray, 0);
    vector<float> ty = kd_IntersectTime(kd_node->ymin, kd_node->ymax, ray, 1);
    vector<float> tz = kd_IntersectTime(kd_node->zmin, kd_node->zmax, ray, 2);
    
    tmin = std::max({tx[0], ty[0], tz[0]});
    tMax = std::min({tx[1], ty[1], tz[1]});
    
    //std::cout<<"tM - tm = "<<tM - tm<<std::endl;
    if(tMax-tmin>=0){
        return true;
    }
    return false;
}


bool Accel::kd_Loop_node(HitInfo& minHit, float* tMin, float* tMax, kd_Node* kd_node, const Ray& ray) const
{
    bool hit;
    HitInfo tempMinHit;
    if(!kd_node->parent)
    {
        hit = false;
        //floor
        if((*m_objects)[m_objects->size()-1]->intersect(tempMinHit, ray))
        {
            hit = true;
            if (tempMinHit.t < minHit.t)
            {
                minHit = tempMinHit;
            }
        }
    }
    
    for(int i=0; i <kd_node->belonging_Triangles.size() ;i++)
    {
        if(kd_node->belonging_Triangles[i]->intersect(tempMinHit, ray,*tMin,*tMax))
           {
               hit = true;
               if (tempMinHit.t < minHit.t){minHit = tempMinHit;}
           }
    }
    
    bool hasLeft=kd_node->left?true:false;
    bool hasRight=kd_node->right?true:false;

    

    
    
    if(hasLeft and kd_intersect_with_node(kd_node->left,ray))
    {
        
        hit =hit | kd_Loop_node(minHit, tMin, tMax, kd_node->left, ray);
        
    }
    if(hasRight and kd_intersect_with_node(kd_node->right,ray))
    {
        hit = hit | kd_Loop_node(minHit, tMin, tMax, kd_node->right, ray);
    }
    
    return hit;
}


