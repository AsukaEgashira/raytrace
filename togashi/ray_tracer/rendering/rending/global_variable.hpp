//
//  global_variable.hpp
//  rending
//
//  Created by togashi on 2017/09/11.
//  Copyright © 2017年 togashi. All rights reserved.
//

#ifndef global_variable_hpp
#define global_variable_hpp

#include <stdio.h>


//root box
extern float left_;
extern float right_;
extern float floor_;
extern float ceil_;
extern float front_;
extern float back_;

//depth of tree
extern int depth_t;

//per x , y, z
extern float dx_;
extern float dy_;
extern float dz_;

extern int TreeType;

#endif /* global_variable_hpp */
