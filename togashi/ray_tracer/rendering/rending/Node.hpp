//
//  Node.hpp
//  rending
//
//  Created by togashi on 2017/09/11.
//  Copyright © 2017年 togashi. All rights reserved.
//

#ifndef Node_hpp
#define Node_hpp

#include <stdio.h>


#include <vector>
//#include "Object.h"

class Object;
class Triangle;

using std::vector;

class Node{
public:
    Node(float ceiling, float floor, float left, float right, float back, float front, int depth);
    ~Node();
    
    float ceiling;
    float floor;
    float left;
    float right;
    float front;
    float back;
    int depth;
    
    //vector<Object*> belong;
    //vector<Object*> candidate;
    vector<int> ob_index;
    
    
    Node* children[8]; //Octree
    Node* parent;
};

class kd_Node
{
public:
    kd_Node(){};
    kd_Node *parent;
    kd_Node *left;//left,right mean children
    kd_Node *right;
    float xmin;
    float xmax;
    float ymin;
    float ymax;
    float zmin;
    float zmax;
    bool isRoot=false;
    
    std::vector<Triangle*> belonging_Triangles;
    std::vector<Triangle*> list;
    
    Object* core;
};



#endif /* Node_hpp */
