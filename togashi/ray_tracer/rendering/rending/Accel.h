#ifndef MIRO_ACCEL_H_INCLUDED
#define MIRO_ACCEL_H_INCLUDED

#include "Miro.h"
#include "Object.h"
#include "Node.hpp"
#include "global_variable.hpp"
#include "Triangle.h"

class Accel
{
public:
    void build(Objects * objs);
    bool intersect(HitInfo& result, const Ray& ray, float tMin = 0.0f, float tMax = MIRO_TMAX) const;
    
    //original
    void OctDevideSpatial(Node* node, float ceiling, float floor, float left, float right, float back, float front, int depth);
    void OctBuild_tree();
    void OctRegister_tri();
    bool OctLoop_node(HitInfo& minHit, float* tMin, float* tMax,Node* node, const Ray& ray) const;
    bool OctRay_aabb_intersect(Node* node, const Ray& ray) const;
    vector<float> OctIntersectTime(float degm, float degM, const Ray& ray, int ver) const;
    
    void build2(Objects* objs);
    
    void test();
    
    Node* root;
    
    //kd tree function
    kd_Node* kd_root;
    void kd_build();
    kd_Node* kd_maketree(Objects* kd_objects,int l,int r,int depth,float xmin,float xmax,float ymin,float ymax,float zmin,float zmax,Object* Core,int direction);

    void registerTriangleToNode(kd_Node* kd_node,Triangle &tri_obj);
    bool kd_isInside(kd_Node &kd_node,Triangle &tri_obj);
    
    bool kd_Loop_node(HitInfo& minHit, float* tMin, float* tMax, kd_Node* kd_node, const Ray& ray) const;
    bool kd_intersect_with_node(kd_Node* kd_node,const Ray& ray)const;
    vector<float> kd_IntersectTime(float degm, float degM, const Ray& ray, int ver) const;

    
    
protected:
    Objects * m_objects;
    //bool tree = true;
    //int TreeType = 2;//0 : 全探索 ,1 : Oct Tree ,2 : kd Tree
};

class axisSorter {
    int k;//k = 0 or 1  0:x座標で分割 1:y座標で分割 2:z座標で分割
public:
    axisSorter(int _k) : k(_k) {}
    double operator()(/*const */Object *a,/*const*/ Object *b) {
        Triangle* tri_a = dynamic_cast<Triangle*>(a);
        Triangle* tri_b = dynamic_cast<Triangle*>(b);
        
//        return 1;
        
        return tri_a->gravity_center(k) < tri_b->gravity_center(k);
        //return a.position.pvec[k] < b.position.pvec[k];
    }
};


#endif // MIRO_ACCEL_H_INCLUDED
