//
//  global_variables.h
//  BaseOpenGL
//
//  Created by togashi on 2017/09/04.
//  Copyright © 2017年 togashi. All rights reserved.
//

#ifndef global_variables_h
#define global_variables_h

#include </Users/togashi/Desktop/BaseOpenGL/glfw/include/GLFW/glfw3.h>

//#define dcout(a) std::cout<<a;//cout for debug.
#define dcout(a)


extern float ratio;
extern int width, height;


extern float dt;

//damp coef for colliding the wall.
extern const float damp;//0.98f;

extern const float collisionDamp;
//coef for friction at the floor.
extern const float friction;//0.98f;

extern const float ceil_,floor_;
extern const float left_,right_;

//max and min velocity
extern const float v_min,v_max;



//very small number.
extern const float epsilon;

extern const int num_particle;//20
extern const bool uniform_density;
extern const bool uniform_radius;
extern const float particle_radius;
extern const float radius_max;
extern const float radius_min;

extern const float gravity_coefficient;
extern const GLfloat external_force_coef;


#endif /* global_variables_h */
