//
//  kd_tree2.hpp
//  BaseOpenGL
//
//  Created by togashi on 2017/09/04.
//  Copyright © 2017年 togashi. All rights reserved.
//

#ifndef kd_tree_test_hpp
#define kd_tree_test_hpp

#include <stdio.h>

void test_tree_construction();

void test_tree();

#endif /* kd_tree2_hpp */
