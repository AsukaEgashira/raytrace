//
//  kd_tree2.cpp
//  BaseOpenGL
//
//  Created by togashi on 2017/09/04.
//  Copyright © 2017年 togashi. All rights reserved.
//

#include "kd_tree_test.hpp"
#include "t_particle.hpp"
#include "kd_tree.h"
#include <vector>
#include <assert.h>


inline double dist(double x1, double y1, double x2, double y2) {
    return (x1-x2) * (x1-x2) + (y1-y2) * (y1-y2);
}

inline double dist(const t_particle &x, const t_particle &y) {
    double ret = 0;
    //for (int i = 0; i < (int)x.position.size(); i++)
    for (int i = 0; i < (int)sizeof(x.position.pvec); i++)
        ret += (x.position.pvec[i] - y.position.pvec[i]) * (x.position.pvec[i] - y.position.pvec[i]);
    return ret;
}

inline double sq(double x) {
    return x*x;
}

t_particle query(t_particle &a,Node *v, int depth) {//aは自分で入力
    int k = depth % (int)sizeof(a.position.pvec);
    if ((v->left != NULL && a.position[k] < v->core.position[k]) ||
        (v->left != NULL && v->right == NULL)) {
        t_particle d = query(a, v->left, depth+1);
        if (dist(v->core, a) < dist(d, a))
            d = v->core;
        if (v->right != NULL && sq(a.position[k] - v->core.position[k]) < dist(d, a)) {
            t_particle d2 = query(a, v->right, depth+1);
            if (dist(d2, a) < dist(d, a))
                d = d2;
        }
        return d;
    }
    else if (v->right != NULL) {
        t_particle d = query(a, v->right, depth+1);
        if (dist(v->core, a) < dist(d, a))
            d = v->core;
        if (v->left != NULL && sq(a.position[k] - v->core.position[k]) < dist(d, a)) {
            t_particle d2 = query(a, v->left, depth+1);
            if (dist(d2, a) < dist(d, a))
                d = d2;
        }
        return d;
    }
    else
        return v->core;
//    switch(k)
//    {
//        case 0:
//            if ((v->left != NULL && a.position.pvec[k] < v->xmax) ||
//                (v->left != NULL && v->right == NULL))
//            {
//                Node d = query(a, v->left, depth+1);
//                for(int x=0;x<v->belonging_particles.size();x++)
//                {
//                    if(dist(a,) < dist(a,d))
//                }
//                
//            }
//    }
}

//void test_registarParticle_output(Node *node,int depth)
//{
//    std::cout<<"depth= "<<depth<<std::endl;
//    std::cout<<"xmin = "<<node.xmin<<std::endl;
//    std::cout<<"xmax = "<<node.xmax<<std::endl;
//    std::cout<<"ymin = "<<node.ymin<<std::endl;
//    std::cout<<"ymax = "<<node.ymax<<std::endl;
//    
//    for(int i=0;node.belonging_particles.size();i++)
//    {
//        std::cout <<"index = "<< node.belonging_particles[i]->index<<std::endl;
//    }
//
//    if(!(node.left == nullptr))
//    {
//        node = node.left;
//        test_registarParticle_output(node,depth+1);
//    }
//    if(!(node.right == nullptr))
//    {
//        node = node.right;
//        test_registarParticle_output(node,depth+1);
//    }
//    
//}

Node test_registarParticle_output(Node *node,int depth)
{
    if(node==nullptr)return NULL;
    std::cout<<"-----depth= "<<depth<<"------"<<std::endl;
    std::cout<<"core = "<<node->core.index<<std::endl;
    std::cout<<"xmin = "<<node->xmin<<std::endl;
    std::cout<<"xmax = "<<node->xmax<<std::endl;
    std::cout<<"ymin = "<<node->ymin<<std::endl;
    std::cout<<"ymax = "<<node->ymax<<std::endl;
    
    for(int i=0; i < node->belonging_particles.size();i++)
    {
        std::cout <<"belonging_index = "<< node->belonging_particles[i]->index<<std::endl;
    }
    
    if(!(node->left == nullptr))
    {
        //node = node->left;
        test_registarParticle_output(node->left,depth+1);
    }
    if(!(node->right == nullptr))
    {
        //node = node->right;
        test_registarParticle_output(node->right,depth+1);
    }
    return node;
}


void test_registarParticleToNode() {
    using namespace std;
    vector<t_particle> test_registarParticles;
    
    
    //example 1 ****************
//    t_particle p0=t_particle({0.01,0.5},{0,0},0,0.1,0);//test_particles[0]の少し右
//    t_particle p1=t_particle({-0.8,0.8},{0,0},0,0.1,1);//1の少し左
//    t_particle p2=t_particle({0.1,0.1},{0,0},0,0.1,2);//中心
//    t_particle p3=t_particle({-0.4,0.0},{0,0},0,0.1,3);//中心より少し左.
//    t_particle p4=t_particle({-0.5,-0.5},{0,0},0,0.05,4);
//    t_particle p5=t_particle({0.5,-0.6},{0,0},0,0.4,5);
//    t_particle p6=t_particle({0.8,0.8},{0,0},0,0.4,6);
    
    
    //example 2 *******************
    t_particle p0=t_particle({0,0},{0,0},0,0.1,0);//test_particles[0]の少し右
    t_particle p1=t_particle({0.3,0.3},{0,0},0,0.1,1);//1の少し左
    t_particle p2=t_particle({0.6,0.6},{0,0},0,0.1,2);//中心
    t_particle p3=t_particle({0.9,0.9},{0,0},0,0.1,3);//中心より少し左.
    t_particle p4=t_particle({-0.3,-0.5},{0,0},0,0.3,4);
    t_particle p5=t_particle({-0.6,-0.6},{0,0},0,0.1,5);
    t_particle p6=t_particle({-0.9,-0.9},{0,0},0,0.5,6);

    
    
    assert((p0==p0));
    assert(! (p0!=p0));
    assert((p0!=p1));
    assert(! (p0==p1));
    
    //particle生成
    
    test_registarParticles.push_back(p0);
    test_registarParticles.push_back(p1);
    test_registarParticles.push_back(p2);
    test_registarParticles.push_back(p3);
    test_registarParticles.push_back(p4);
    test_registarParticles.push_back(p5);
    test_registarParticles.push_back(p6);

//    for(int i=0;i<test_registarParticles.size();i++)
//    {
//        std::cout <<"index = "<< test_registarParticles[i].index<<std::endl;
//    }
    
    //木生成
    //Node *root=makeKDTree(target_particle.size(), 0, target_particle.size(), 0);
    Node *root;
    root = makeKDTree(test_registarParticles, 0, test_registarParticles.size(), 0, left_, right_, ceil_, floor_, nullptr, -1);
    
    //registerParticleToNode(root, target_partices);
    
//    for(int i = 0; i < test_registarParticles.size(); i++)//Nodeにballを登録 ballを順に
//    {
//        std::cout << "called particle index order ="<<test_registarParticles[i].index <<std::endl;
//        std::cout << " x =" << test_registarParticles[i].position.pvec[0] << std::endl;
//    }
//
//    std::cout << "indexs...before" << std::endl;
//    for(int i=0;i<7;i++){
//        std::cout << test_registarParticles[i].index << ", ";
//    }
//    std::cout << std::endl;
    
    for (int i = 0; i < test_registarParticles.size(); i++)//Nodeにballを登録 ballを順に
    {
        registerParticleToNode(root,test_registarParticles[i]);//Nodeにballを登録
    }
    
//    std::cout << "indexs...after" << std::endl;
//    for(int i=0;i<7;i++){
//        std::cout << test_registarParticles[i].index << ", ";
//    }
//    std::cout << std::endl;
    //registerParticleToNode(root,test_registarParticles);
    
    //1つずつのparticleが狙ったNodeに入っているか確かめる。
    //rootからleftとかrightにどんどんおりていく。node->left==nullptrじゃない限り.
    //nodeごとに以下をやる。
    //assert(node->belonging_particlesがpiを含む) //findとか調べてください
    
    //Node node = root;
    
    test_registarParticle_output(root,0);
    
    //bool existance = true;
    
    //while(existance)
    //{
      //  test_registarParticle_output(node);
        
        
    //}
    //Node n1=n0->left;
    //Node n2=n0->right;
    
    
    
    
}


void test_isInside(){
    
    t_particle p0=t_particle({0.01,0.5},{0,0},0,0.1);//test_particles[0]の少し右
    t_particle p1=t_particle({-0.8,0.8},{0,0},0,0.1);//1の少し左
    t_particle p2=t_particle({0.0,0.0},{0,0},0,0.1);//中心
    t_particle p3=t_particle({-0.4,0.0},{0,0},0,0.1);//中心より少し左.

    t_particle p4=t_particle({-0.5,-0.5},{0,0},0,0.05);
    t_particle p5=t_particle({-0.5,-0.5},{0,0},0,0.4);

    
    
    Node *n0=new Node(true);//root
    
    assert(isInside(*n0, p0));
    assert(isInside(*n0, p1));
    assert(isInside(*n0, p2));
    assert(isInside(*n0, p3));
    
    Node *n1=new Node(false);//rootではない
    assert(isInside(*n1, p0));
    assert(!isInside(*n1, p1));
    assert(isInside(*n1, p2));
    assert(isInside(*n1, p3));
    
    
    Node *n2=new Node(left_,0.1,ceil_,-0.2);
    assert(!isInside(*n2, p0));
    assert(!isInside(*n2, p1));
    assert(!isInside(*n2, p2));
    assert(!isInside(*n2, p3));
    assert(isInside(*n2, p4));
    assert(!isInside(*n2, p5));

    

    
    
   // std::cout<<registarParticle(*n0, p0, 0);
    
}



void test_tree_construction(){
    using namespace std;
    vector<t_particle> test_particles;
    test_particles.push_back(t_particle({0,0.5},{0,0},0,0.1));
    test_particles.push_back(t_particle({-0.3,0.8},{0,0},0,0.1));
    test_particles.push_back(t_particle({0.3,0},{0,0},0,0.1));
    test_particles.push_back(t_particle({-0.8,-0.4},{0,0},0,0.1));
    test_particles.push_back(t_particle({0.2,-0.05},{0,0},0,0.1));


    //最近某探索
    Node *root;
    //root= makeKDTree(test_particles, 0, test_particles.size(), 0);
    root = makeKDTree(test_particles, 0, test_particles.size(), 0, left_, right_, ceil_, floor_, nullptr, -1);
    
    t_particle p0=t_particle({0.01,0.5},{0,0},0,0.1);//test_particles[0]の少し右
    t_particle nearest =query(p0, root, 0);
    std::cout<<nearest.position[0]<<","<<nearest.position[1]<<std::endl;
    
    t_particle p1=t_particle({-0.8,0.8},{0,0},0,0.1);//1の少し左
    nearest =query(p1, root, 0);
    std::cout<<nearest.position[0]<<","<<nearest.position[1]<<std::endl;
    
    
    t_particle p2=t_particle({0.0,0.0},{0,0},0,0.1);//中心
    nearest =query(p2, root, 0);
    std::cout<<nearest.position[0]<<","<<nearest.position[1]<<std::endl;
    
    
    t_particle p3=t_particle({-0.4,0.0},{0,0},0,0.1);//中心より少し左.
    nearest =query(p3, root, 0);
    std::cout<<nearest.position[0]<<","<<nearest.position[1]<<std::endl;
    
};

void make_candidate(Node *node)
{
    std::cout << "make_candidate"<<std::endl;
    for(int i=0;i < node->belonging_particles.size();i++)
    {
        node->list.push_back(node->belonging_particles[i]);
    }
    std::cout << "make_candidate　2"<<std::endl;
    std::cout << node->isRoot << std::endl;
    if(!node->isRoot)
    {
        for(int i=0;i < node->parent->list.size();i++)
        {
            node->list.push_back(node->parent->list[i]);
        }
    }
    std::cout << "make_candidate　3"<<std::endl;
    
    if(node->left){make_candidate(node->left);}
    if(node->right){make_candidate(node->right);}
}

void check_collision(Node* node)
{
    for(int i=0;i < node->belonging_particles.size();i++)
    {
        dcout("belonging particle index i =");
        dcout(node->belonging_particles[i]->index);
        dcout("-------------");
        dcout("\n");
        
        for(int j=i+1;j < node->list.size();j++)
        {
            node->belonging_particles[i]->collide(*(node->list[j]));
            dcout("list particle index");
            dcout(node->list[j]->index);
            dcout("\n");
        }
    }
    if(node->left){check_collision(node->left);}
    if(node->right){check_collision(node->right);}
}


void test_collision_candidate(Node *node)
{
    make_candidate(node);
    check_collision(node);
    
}



void test_collision()
{
    using namespace std;
    vector<t_particle> test_registarParticles;
    
    //example 1 ****************
        t_particle p0=t_particle({0.01,0.5},{0,0},0,0.1,0);//test_particles[0]の少し右
        t_particle p1=t_particle({-0.8,0.8},{0,0},0,0.1,1);//1の少し左
        t_particle p2=t_particle({0.1,0.1},{0,0},0,0.1,2);//中心
        t_particle p3=t_particle({-0.4,0.0},{0,0},0,0.1,3);//中心より少し左.
        t_particle p4=t_particle({-0.5,-0.5},{0,0},0,0.05,4);
        t_particle p5=t_particle({0.5,-0.6},{0,0},0,0.4,5);
        t_particle p6=t_particle({0.8,0.8},{0,0},0,0.4,6);
    
    
    //example 2 *******************
//    t_particle p0=t_particle({0,0},{0,0},0,0.1,0);//test_particles[0]の少し右
//    t_particle p1=t_particle({0.3,0.3},{0,0},0,0.1,1);//1の少し左
//    t_particle p2=t_particle({0.6,0.6},{0,0},0,0.1,2);//中心
//    t_particle p3=t_particle({0.9,0.9},{0,0},0,0.1,3);//中心より少し左.
//    t_particle p4=t_particle({-0.3,-0.5},{0,0},0,0.3,4);
//    t_particle p5=t_particle({-0.6,-0.6},{0,0},0,0.1,5);
//    t_particle p6=t_particle({-0.9,-0.9},{0,0},0,0.5,6);

    //particle生成
    test_registarParticles.push_back(p0);
    test_registarParticles.push_back(p1);
    test_registarParticles.push_back(p2);
    test_registarParticles.push_back(p3);
    test_registarParticles.push_back(p4);
    test_registarParticles.push_back(p5);
    test_registarParticles.push_back(p6);
    
     //木生成
    Node *root;
    root = makeKDTree(test_registarParticles, 0, test_registarParticles.size(), 0, left_, right_, ceil_, floor_, nullptr, -1);
    root->isRoot=true;
    
    //Nodeにballを登録 ballを順に
    for (int i = 0; i < test_registarParticles.size(); i++)
    {
        registerParticleToNode(root,test_registarParticles[i]);
    }
    
    test_registarParticle_output(root,0);
    
    test_collision_candidate(root);



}


void test_tree(){
    //test_tree_construction();
    //test_isInside();
    
    //test_registarParticleToNode();
    test_collision();
    
}
