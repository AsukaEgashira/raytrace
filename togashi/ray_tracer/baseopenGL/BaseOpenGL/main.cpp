

#include "global_variables.h"

#include "kd_tree.h"
#include "kd_tree_test.hpp"


float ratio;
int width, height;

float dt = 0.1;

//damp coef for colliding the wall.
const float damp=1.f;//0.98f;

const float collisionDamp=0.95f;
//coef for friction at the floor.
const float friction=1.f;//0.98f;

const float ceil_=-0.9f,floor_=0.9f;
const float left_=-0.9f,right_=0.9f;

//max and min velocity
const float v_min=-1.f,v_max=1.f;



//very small number.
const float epsilon=0.001f;

const int num_particle=10000;//20
const bool uniform_density=1;
const bool uniform_radius=0;
const float particle_radius=0.01f;//0.03f;
const float radius_max=uniform_radius?particle_radius: 0.02f;
const float radius_min=uniform_radius?particle_radius: 0.01f;

const float gravity_coefficient=0.98f;
const GLfloat external_force_coef=uniform_density?0.01f:1.f;

std::vector<t_particle> particles;


bool withTree=1;

inline void handleCollision(){
    switch (withTree) {
        case 0://default
            for(int i=0;i!=particles.size();++i)
            {
                for(int j=i+1;j!=particles.size();++j)
                {
                    particles[i].collide(particles[j]);
                }
            }
        case 1://kdtree
            kdtree(particles);
            break;
            
        default:
            break;
    }
    
}


inline void drawFrame(){
    static const GLfloat vtx3[] = {
        left_, ceil_,
        left_, floor_,
        right_, floor_,
        right_, ceil_,
    };
    
    glVertexPointer(2, GL_FLOAT, 0, vtx3);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    
    glEnable(GL_MULTISAMPLE);
    glMatrixMode(GL_MODELVIEW);
    
    //glDrawArrays has to be sandwiched by glEnableCliantState and glDisableCliantState.
    glEnableClientState(GL_VERTEX_ARRAY);
    
    glPushMatrix();
    glDrawArrays(GL_QUADS, 0, 4);
    glPopMatrix();
    
    glDisableClientState(GL_VERTEX_ARRAY);
}

inline void update(){
    
    for( auto it=particles.begin();it!=particles.end();it++){
        it->update();;
    }
    handleCollision();
    //kdtree();
    
}

inline void drawParticles(){
    glColor3f(0.f, 0.f, 1.f);
    float sizePx;
    
    glEnable(GL_POINT_SMOOTH);
    if(uniform_radius){
        sizePx=(height*particle_radius/(1.f+1.f)*2.f);
        glPointSize( sizePx );
        
        glBegin(GL_POINTS);
        for( auto &particle:particles){
            
            particle.draw();
            
        }
        glEnd();
        
    }else{
        
    }
    for( auto &particle:particles){
        
        sizePx=(height*particle.radius/(1.f+1.f)*2.f);
        
        glPointSize( sizePx );
        
        glBegin(GL_POINTS);
        
        particle.draw();
        
        
        glEnd();
    }
    
}


void generate_particles(){
    
    //Generate particles.
    //Initial positions and velocities are given by Mersenne twister.
    
    static std::random_device rd;
    //Random number generater using Mersenne twister.
    static std::mt19937 engine(rd()) ;
    
    //Generate positions and velocities under uniform distribution.
    std::uniform_real_distribution<GLfloat> distribution_x( left_,  right_) ;
    std::uniform_real_distribution<GLfloat> distribution_y( ceil_,  floor_) ;
    std::uniform_real_distribution<GLfloat> distribution_vx( v_min,  v_max) ;
    std::uniform_real_distribution<GLfloat> distribution_vy( v_min,  v_max) ;
    
    std::uniform_real_distribution<GLfloat> distribution_r( radius_min,  radius_max) ;
    
    for(int i=0;i<num_particle;i++){
        float x=distribution_x(engine);
        float y=distribution_y(engine);
        float vx=distribution_vx(engine);
        float vy=distribution_vy(engine);
        
        float r=distribution_r(engine);
        
        float mass=uniform_density?1.f*r*r:1.f;
        
        
        particles.push_back((t_particle(PVector(x, y), PVector(vx, vy), mass,r)));
        particles[i].index = i;
    }
}

inline void addForce(PVector external_force){
    for(auto it=particles.begin();it!=particles.end();it++){
        it->addForce(external_force);
    }
}

const float goToAndFrom_speed=0.1f;//0.1f;
inline void goToPoint(PVector point){
    for(auto it=particles.begin();it!=particles.end();it++){
        PVector direction=point-it->position;
        
        //it->force=it->force+goToAndFrom_speed*direction;
        it->velocity+=goToAndFrom_speed*direction;
    }
}

inline void goFromPoint(PVector point){
    for(auto it=particles.begin();it!=particles.end();it++){
        PVector direction=point-it->position;
        float distance=direction.norm();
        
        //        it->force=it->force-goToAndFrom_speed*direction;
        it->velocity-=goToAndFrom_speed*direction/(distance*distance);
        
        
    }
}

static void clear(){
    particles.clear();
}



void render (GLFWwindow * window){
    
    
    
    glfwGetFramebufferSize(window, &width, &height);
    ratio = width / (float)height;
    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT);
    
    //ウインドウの引き伸ばしに不変にする。描画範囲の設定
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    
    //If setting up, glOrtho(0.0f, width, 0.0f, height, -1.0f, 1.0f);
    //then the window size coincides with the inner scale.
    glOrtho(-ratio, ratio, -1.f, 1.f, 1.f, -1.f);
    
    
    
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glPushMatrix();
    drawFrame();
    glPopMatrix();
    
    glPushMatrix();
    glRotatef(180, 1, 0, 0);
    drawParticles();
    glPopMatrix();
    
    
    glfwSwapBuffers(window);
    glfwPollEvents();
}

static void error_callback(int error, const char* description)
{
    fputs(description, stderr);
}



static void key_callback(GLFWwindow* window, int key, int scancode, int action,
                         int mods)
{
    
    if ( key==GLFW_KEY_A  &&action==GLFW_PRESS){
        generate_particles();
    }
    
    
    if ( (key==GLFW_KEY_R or key==GLFW_KEY_0)  &&action==GLFW_PRESS){
        clear();
        generate_particles();
    }
    
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
    PVector ex_force(0,0);
    if (key==GLFW_KEY_RIGHT ){
        ex_force+=PVector(external_force_coef,0);
    }
    if (key==GLFW_KEY_LEFT ){
        ex_force+=PVector(-external_force_coef,0);
    }
    if (key==GLFW_KEY_DOWN ){
        ex_force+=PVector(0,external_force_coef);
    }
    if (key==GLFW_KEY_UP ){
        ex_force+=PVector(0,-external_force_coef);
    }
    
    if(action==GLFW_PRESS){
        addForce(ex_force);
    }else if (action==GLFW_RELEASE){
        addForce(-ex_force);
    }
}

static void mouse_input(GLFWwindow*window){
    
    if( glfwGetMouseButton(window,GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS ) {
        double mousex, mousey;
        glfwGetCursorPos(window,&mousex, &mousey);
        
        //Translate the mouse coordinate (mousex,mousey) in [0,width]*[0,height]
        //to target position in [-1,1]*[-,1,1].
        int width, height;
        glfwGetFramebufferSize(window, &width, &height);
        GLfloat x,y;
        GLfloat W=width/2,H=height/2;
        x=(mousex-W)/H;
        y=(mousey/height-0.5)*2;
        PVector target_position(x,y);
        goToPoint(target_position);
        
    }
    
    if( glfwGetMouseButton(window,GLFW_MOUSE_BUTTON_LEFT) == GLFW_RELEASE ) {
        
    }
    
    
    
    if( glfwGetMouseButton(window,GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS ) {
        double mousex, mousey;
        glfwGetCursorPos(window,&mousex, &mousey);
        
        //Translate the mouse coordinate (mousex,mousey) in [0,width]*[0,height]
        //to target position in [-1,1]*[-,1,1].
        int width, height;
        glfwGetFramebufferSize(window, &width, &height);
        GLfloat x,y;
        GLfloat W=width/2,H=height/2;
        x=(mousex-W)/H;
        y=(mousey/height-0.5)*2;
        PVector target_position(x,y);
        goFromPoint(target_position);
        
    }
    if( glfwGetMouseButton(window,GLFW_MOUSE_BUTTON_RIGHT) == GLFW_RELEASE ) {
        
    }
    
}



int main(void)
{
    //test_tree();exit(0);
    
    GLFWwindow* window;
    glfwSetErrorCallback(error_callback);
    if (!glfwInit())
        exit(EXIT_FAILURE);
    window = glfwCreateWindow(640, 480, "Simple example", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);
    
    glfwSetKeyCallback(window, key_callback);
    
    
    //Timer setting
    double FPS = 60;
    double currentTime, lastTime, elapsedTime;
    currentTime = lastTime = elapsedTime = 0.0;
    glfwSetTime(0.0); //Initialize timer.
    
    dt=1/FPS;
    
    generate_particles();

    
    
    
    
    while (!glfwWindowShouldClose(window))
    {
        
        glClearColor(0.3f, .3f, 0.3f, 0.f);
        currentTime = glfwGetTime();
        elapsedTime = currentTime - lastTime;
        
        if (elapsedTime > 1.0/FPS) {
            
            mouse_input(window);
            
            update();
            
            render(window);
            
            lastTime = glfwGetTime();
        }
        
    }
    glfwDestroyWindow(window);
    glfwTerminate();
    exit(EXIT_SUCCESS);
}
