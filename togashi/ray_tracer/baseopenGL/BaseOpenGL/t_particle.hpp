//
//  t_particle.hpp
//  BaseOpenGL
//
//  Created by togashi on 2017/09/04.
//  Copyright © 2017年 togashi. All rights reserved.
//

#ifndef t_particle_hpp
#define t_particle_hpp

#include <stdio.h>
#include <vector>
#include <algorithm>
#include <cmath>

#include </Users/togashi/Desktop/BaseOpenGL/glfw/include/GLFW/glfw3.h>

#include "global_variables.h"

using std::sqrtf;

struct PVector{
    //We have not written code to free memory.
    
    GLfloat pvec[2];
    
    inline PVector(const GLfloat x, const GLfloat y){
        pvec[0]=x;
        pvec[1]=y;
    }
    
    inline PVector(const GLfloat scalar){
        pvec[0]=scalar;
        pvec[1]=scalar;
    }
    
    //PVector(){}
    
    inline GLfloat& operator[](const int index){
        return pvec[index];
    }
    inline PVector operator*(const GLfloat scalar)const {
        return PVector(scalar*pvec[0],scalar*pvec[1]);
    }
    
    inline PVector operator/(const GLfloat scalar)const {
        return PVector(1.f/scalar*pvec[0],1.f/scalar*pvec[1]);
    }
    
    inline PVector operator+(const PVector  anotherVector) const{
        return PVector(this->pvec[0]+anotherVector.pvec[0],this->pvec[1]+anotherVector.pvec[1]);
    }
    
    inline PVector operator-(void) const{
        return PVector(-pvec[0],-pvec[1]);
    }
    
    inline PVector operator-(const PVector&  anotherVector)const{
        return *this+(-anotherVector);
    }
    
    inline void operator+=(const PVector & anotherVector){
        pvec[0]+=anotherVector.pvec[0];
        pvec[1]+=anotherVector.pvec[1];
        return;
    }
    
    
    inline void operator-=(const PVector & anotherVector){
        pvec[0]-=anotherVector.pvec[0];
        pvec[1]-=anotherVector.pvec[1];
        return;
    }
    
    inline void operator*=(const GLfloat scalar){
        pvec[0]*=scalar;
        pvec[1]*=scalar;
        return;
    }
    
    inline float dot(const PVector anotherVector)const{
        return pvec[0]*anotherVector.pvec[0]+pvec[1]*anotherVector.pvec[1];
        
    }
    
    inline GLfloat norm(){
        return sqrtf(pvec[0]*pvec[0]+pvec[1]*pvec[1]);
    }
    
    ~PVector(){
    }
};

inline PVector operator*(const GLfloat scalar, PVector aVector){
    return aVector*scalar;
}


const PVector gravity=PVector(0, gravity_coefficient);




class t_particle {
public:
    int index=-1;
    PVector position;
    PVector velocity;
    PVector force;
    PVector original_force;
    float radius;
    float mass;
    float duration;//duration of existance. Not used now.
    
    t_particle(PVector position, PVector velocity, float mass,float radius=particle_radius,int index=-1):position(position),velocity(velocity),mass(mass),force(gravity*mass),original_force(force),radius(radius),index(index) {
        
    }
    
    inline void resetForce(){
        force=original_force;
    }
    
    inline void draw(){
        //sizePx=(height*particle_radius/(1.f+1.f)*2.f);
        
        glVertex2f(position[0] , position[1]);
    }
    
    inline void addForce(PVector external_force){
        force=force+external_force;
    }
    
    //Verlet integration
    inline void update(){
        
        auto prevPosition=position;
        
        velocity=velocity+dt/mass*force;
        position=position+velocity*dt;
        
        if (position[0]>right_ ) {
            velocity[0]=-damp*fabs(velocity[0]);
            
        } else if (position[0]<left_ ) {
            velocity[0]=damp*fabs(velocity[0]);
        }
        
        if (position[1]>floor_ -epsilon) {
            position[1]=floor_;
            velocity[1]=-damp*fabs(velocity[1]);
        } else if (position[1]<ceil_ ) {
            velocity[1]=+damp*fabs(velocity[1]);
        }
        
        //friction
        if(prevPosition[1]>floor_-epsilon&& position[1]>floor_-epsilon){
            velocity[0]=friction* velocity[0];
        }
    }
    
    inline bool detectCollide(t_particle &particle){
        float dist=(position-particle.position).norm();
        if(dist<radius+particle.radius){
            return true;
        }
        return false;
    }
    
    inline void respondCollide(t_particle &particle){
        PVector diff=position-particle.position;
        PVector k(diff/diff.norm());
        float a=2*k.dot(velocity-particle.velocity )/(1/mass+1/particle.mass);
        velocity=collisionDamp*(velocity-(a/mass)*k);
        particle.velocity=collisionDamp*(particle.velocity+(a/particle.mass)*k);
    }
    
    //This function handles both detect and response to collision.
    inline bool collide(t_particle &particle)
    {
        bool isColliding=detectCollide(particle);
        if(isColliding){
            respondCollide(particle);
            //printf("+++++++++++******");

        }
        //printf("+++++++++++******");
        return isColliding;
    }
    
    inline bool collide(std::vector<t_particle*> &belonging_particles)
    {
        bool isColliding = false;//初期化は必要ないけど一応
        for(int i = 0; i<belonging_particles.size();++i)
        {
            isColliding=detectCollide(*belonging_particles[i]);
            if(isColliding)
            {
                respondCollide(*belonging_particles[i]);
            }
        }
        return isColliding;
    }
    
    const bool operator == ( const t_particle& another_particle ) const{
        if(index==-1){return false;}
        
        if(index==another_particle.index){return true;}
        
        return false;
    };
    
    const bool operator != ( const t_particle& another_particle ) const{
        return ! (*this==(another_particle));
    };
    
};

#endif /* t_particle_hpp */
