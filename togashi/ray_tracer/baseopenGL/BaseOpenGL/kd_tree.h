#ifndef KD_TREE
#define KD_TREE

#include </Users/togashi/Desktop/BaseOpenGL/glfw/include/GLFW/glfw3.h>
//#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <vector>
# include <random>
#include <iostream>
#include <algorithm>

#include "global_variables.h"

#include "t_particle.hpp"

using std::vector;
using std::fabs;





//------------------------------------

struct Node
{
public:
    Node *parent;
    Node *left;//left,right mean children
    Node *right;
    t_particle core;
    bool isRoot=false;
    
    double xmin = left_;
    double xmax = right_;
    double ymin = ceil_;
    double ymax = floor_;
    o
    Node(bool isRoot):isRoot(isRoot),core(t_particle(PVector(-10,-10), PVector(0,0), -1,-1)){};
    
    Node(t_particle core,bool isRoot=false):isRoot(isRoot),core(core){}
    Node(t_particle core, double xmin, double xmax,double ymin, double ymax,bool isRoot=false):isRoot(isRoot),core(core),xmin(xmin),xmax(xmax),ymin(ymax),ymax(ymin){}
    
    Node( double xmin, double xmax,double ymin, double ymax,bool isRoot=false):isRoot(isRoot),core(t_particle(PVector(-10,-10), PVector(0,0), -1,-1)),xmin(xmin),xmax(xmax),ymin(ymin),ymax(ymax){}
    
    Node():isRoot(false),core(t_particle(PVector(-10,-10), PVector(0,0), -1,-1)){};
    
    
};

class axisSorter {
    int k;//k = 0 or 1  0:x座標で分割 1:y座標で分割
public:
    axisSorter(int _k) : k(_k) {}
    double operator()(const t_particle &particle_a, const t_particle &particle_b) {
        return particle_a.position.pvec[k] < particle_b.position.pvec[k];
    }
};


inline bool isInside(const Node &node,const t_particle &ball){

    
    if(node.isRoot){return true;}
    
    bool ans=false;
    
    //std::cout << "after isRoot  index" << ball.index << std::endl;
    
    if((node.xmin < (ball.position.pvec[0] -ball.radius)) &&  ((ball.position.pvec[0] + ball.radius) < node.xmax))
    {
       if((node.ymin < (ball.position.pvec[1] -ball.radius)) &&  ((ball.position.pvec[1] + ball.radius) < node.ymax))
              {
            ans = true;
//                  std::cout << "particle index : " << ball.index << std::endl;
//                  std::cout << "    position : " << ball.position.pvec[0] << ", "
//                  << ball.position.pvec[1] << std::endl;
//                  
//                  std::cout << "box : " << node.xmin << ", " <<
//                  node.xmax << ", " << node.ymin << ", " << node.ymax
//                  << std::endl;
//                  std::cout << "/*************************/" <<std::endl;
        }
    }
    
    return ans;
}

//inline bool registarParticle(const Node &children,const vector<Data> &ball)//
inline bool isInsideLeft(const Node &node,const t_particle &ball,int left_or_right)
{
    
    bool ans = false;
    switch (left_or_right)
    {
        case 0:
            if(!node.left){return false;}
            if((node.left->xmin < (ball.position.pvec[0] -ball.radius)) &&  ((ball.position.pvec[0] + ball.radius) < node.left->xmax))
            {
                if((node.left->ymin < (ball.position.pvec[1] -ball.radius)) &&  ((ball.position.pvec[1] + ball.radius) < node.left->ymax))
                {
                    ans = true;
                }
            }
            break;
        case 1:
            if(!node.right){return false;}
            if((node.right->xmin < (ball.position.pvec[0] -ball.radius)) &&  ((ball.position.pvec[0] + ball.radius) < node.right->xmax))
            {
                if((node.right->ymin < (ball.position.pvec[1] -ball.radius)) &&  ((ball.position.pvec[1] + ball.radius) < node.right->ymax))
                {
                    ans = true;
                }
            }
            break;
    }
    return ans;
}

Node *makeKDTree(vector<t_particle> &particles, int l, int r, int depth,double xmin,double xmax,double ymin,double ymax,t_particle* Core,int direction);


inline void registerParticleToNode(Node *node,t_particle &particle)
{
    
    if(node->core.index == particle.index)
    {
        node->belonging_particles.push_back(&particle);
        return;
    }
    
     //std::cout << "check particles  index" << particle.index << std::endl;

        //bool inside = true;
        
        bool inside_left=false;
        bool inside_right=false;
            
    if(particle.index == 0){
        std::cout <<"";
    }
    
    if(particle.index==-1){
    
        std::cout<<"";
    }
    
    
    if(node->right ){
        inside_right=isInside(*(node->right),particle);
    }
    
    if(node->left ){
        inside_left=isInside(*(node->left),particle);
    }
    
        //if(inside_left){node=node->left;break;}
        if(inside_left == true){registerParticleToNode(node->left,particle);}
        //if(inside_right){node=node->right;break;}
    
        if(inside_right == true){registerParticleToNode(node->right, particle);}
            
        if(inside_left==false && inside_right==false)
        {
            node->belonging_particles.push_back(&particle);
            //inside = false;
            return;
        }
    
}


void real_collision(Node *node);
void real_check_collision(Node* node);
void real_make_candidate(Node *node);
void delete_root(Node *node);

inline void kdtree(std::vector<t_particle> &target_particles)
{
    //Node *root;
    Node *root = new Node;
    
    root = makeKDTree(target_particles, 0, target_particles.size(), 0, left_, right_, ceil_, floor_, nullptr, -1);
    root->isRoot=true;
    
    //Nodeにballを登録 ballを順に
    for (int i = 0; i < target_particles.size(); i++)
    {
        registerParticleToNode(root,target_particles[i]);
    }
    
    real_collision(root);
    
    delete_root(root);

}











#endif
