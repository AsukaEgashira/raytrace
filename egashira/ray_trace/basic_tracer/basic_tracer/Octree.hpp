////
////  Octree.hpp
////  basic_tracer
////
////  Created by asuka egashira on 2017/09/11.
////  Copyright © 2017年 asuka egashira. All rights reserved.
////
//
//#ifndef Octree_hpp
//#define Octree_hpp
//
//#include <stdio.h>
//#include "Tree.h"
////#include "Object.h"
//#include "Triangle.h"
//#include "Node.hpp"
//#include <vector>
//#include "Miro.h"
//
//using std::vector;
//
//class Octree : public Tree
//{
//public:
//    Octree();
//    virtual ~Octree();
//    virtual void make_tree(Node* node, Objects* objects);
//    void devideSpatial(Node* node, float ceil, float floor, float left, float right, float back, float front, int depth);
//    virtual void register_mesh(Triangle* tri);
//    int calcurate_morton(int x, int y, int z);
//    int bitsep(int b_s, int dim);
//    bool loop_node(HitInfo& minHit, float* tMin, float* tMax,Node* node, const Ray& ray)const;
//    bool intersect_ray_aabb(Node* node, const Ray& ray)const;
//    vector<float> intersectTime(float degm, float degM, const Ray& ray, int ver)const;
//    Node* root;
//};
//
//#endif /* Octree_hpp */
