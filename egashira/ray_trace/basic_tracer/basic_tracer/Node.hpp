//
//  Node.hpp
//  basic_tracer
//
//  Created by asuka egashira on 2017/09/06.
//  Copyright © 2017年 asuka egashira. All rights reserved.
//

#ifndef Node_hpp
#define Node_hpp

#include <stdio.h>
#include <vector>
//#include "Object.h"

using std::vector;

class Node{
public:
    Node(float ceiling, float floor, float left, float right, float back, float front, int depth);
    ~Node();
    
    float ceiling;
    float floor;
    float left;
    float right;
    float front;
    float back;
    int depth;
    
    //vector<Object*> belong;
    //vector<Object*> candidate;
    vector<int> ob_index;
    
    
    Node* children[8]; //Octree
    Node* parent;
};
#endif /* Node_hpp */
