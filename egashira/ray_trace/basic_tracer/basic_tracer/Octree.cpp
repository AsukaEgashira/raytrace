////
////  Octree.cpp
////  basic_tracer
////
////  Created by asuka egashira on 2017/09/11.
////  Copyright © 2017年 asuka egashira. All rights reserved.
////
//
//#include "Octree.hpp"
//#include "global_variable.hpp"
//#include <Eigen/Dense>
//#include <igl/readOBJ.h>
//#include "TriangleMesh.h"
//#include "Ray.h"
//
//Eigen::MatrixXd V;
//Eigen::MatrixXi F;
//
//Octree::Octree(){
//    root = new Node(ceil_, floor_, left_, right_, back_, front_, 0);
//}
//
//Octree::~Octree(){
//
//}
//
//void
//Octree::make_tree(Node* node, Objects* objects){
//    //get max and min of width, height, depth
//    //bool canread =
//    if(objects->size()<600){
//        igl::readOBJ("/Users/asuka/raytrace/egashira/ray_trace/data" "/teapot.obj", V, F);
//    }
//    else if (objects->size()<70000){
//        igl::readOBJ("/Users/asuka/raytrace/egashira/ray_trace/data" "/bunny.obj", V, F);
//    }
//    else{
//        igl::readOBJ("/Users/asuka/raytrace/egashira/ray_trace/data" "/sponza.obj", V, F);
//    }
//    
//    //std::cout << "Can read? " << canread << std::endl;
//    
//    Eigen::Vector3d m = V.colwise().minCoeff();
//    Eigen::Vector3d M = V.colwise().maxCoeff();
//    
//    left_ = m(0);
//    right_ = M(0);
//    floor_ = m(1);
//    ceil_ = M(1);
//    front_ = m(2);
//    back_ = M(2);
//    
//    dx_ = (right_ - left_) / std::pow(2.0, depth_t);
//    dy_ = (ceil_ - floor_) / std::pow(2.0, depth_t);
//    dz_ = (back_ - front_) / std::pow(2.0, depth_t);
//    
//    root = new Node(ceil_, floor_, left_, right_, back_, front_, 0);
//    root->parent = nullptr;
//    
//    std::cout<<"root :"<<std::endl;
//    std::cout<<root->left<<", "<<root->right<<", "<<root->floor<<", "
//    <<root->ceiling<<", "<<root->front<<", "<<root->back<<std::endl;
//    
//    devideSpatial(root, ceil_, floor_, left_, right_, back_, front_, 1);
//    
//    //    std::cout<<"**************"<<std::endl;
//    //    std::cout<<"devide spatial"<<std::endl;
//    //    std::cout<<"**************"<<std::endl;
//
//}
//
//void
//Octree::devideSpatial(Node* node, float ceil, float floor, float left, float right, float back, float front, int depth){
//    if(depth > depth_t){
//        for(int i=0; i<8; i++){
//            node->children[i] = nullptr;
//        }
//        return;
//    }
//    
//    float mid_h = (ceil + floor)/2, mid_w = (right + left)/2, mid_d = (back + front)/2;
//    
//    float x_max[8] = {mid_w, right, mid_w, right, mid_w, right, mid_w, right};
//    float x_min[8] = {left, mid_w, left, mid_w, left, mid_w, left, mid_w};
//    float y_max[8] = {mid_h, mid_h, ceil, ceil, mid_h, mid_h, ceil, ceil};
//    float y_min[8] = {floor, floor, mid_h, mid_h, floor, floor, mid_h, mid_h};
//    float z_max[8] = {mid_d, mid_d, mid_d, mid_d, back, back, back, back};
//    float z_min[8] = {front, front, front, front, mid_d, mid_d, mid_d, mid_d};
//    
//    for(int i=0;i<8;i++){
//        node->children[i] = new Node(y_max[i], y_min[i], x_min[i], x_max[i], z_max[i], z_min[i], depth);
//        node->children[i]->parent = node;
//        
//        //        std::cout<<"children " << i << " : "<<std::endl;
//        //        std::cout<<node->children[i]->left<<", "<<node->children[i]->right<<", "<<node->children[i]->floor<<", "
//        //        <<node->children[i]->ceiling<<", "<<node->children[i]->front<<", "<<node->children[i]->back<<std::endl;
//        
//        int c_dep = depth + 1;
//        devideSpatial(node->children[i], y_max[i], y_min[i], x_min[i], x_max[i], z_max[i], z_min[i], c_dep);
//    }
//}
//
//void
//Octree::register_mesh(Triangle* tri){
//    //0. position aabb
//    TriangleMesh::TupleI3 ti3 = tri->m_mesh->vIndices()[tri->m_index];
//    const Vector3 & v0 = tri->m_mesh->vertices()[ti3.x]; //vertex a of triangle
//    const Vector3 & v1 = tri->m_mesh->vertices()[ti3.y]; //vertex b of triangle
////    const Vector3 & v2 = tri->m_mesh->vertices()[ti3.z]; //vertex c of triangle
//    float x_max = std::max({v0.x, v1.x, v2.x});
//    float x_min = std::min({v0.x, v1.x, v2.x});
//    float y_max = std::max({v0.y, v1.y, v2.y});
//    float y_min = std::min({v0.y, v1.y, v2.y});
//    float z_max = std::max({v0.z, v1.z, v2.z});
//    float z_min = std::min({v0.z, v1.z, v2.z});
//    
//    //    if(this->m_index % 30 ==0){
//    //        std::cout<< this->m_index << " : " << std::endl;
//    //        std::cout << v0.x << ", "<< v0.y << ", "<< v0.z << std::endl;
//    //        std::cout << v1.x << ", "<< v1.y << ", "<< v1.z << std::endl;
//    //        std::cout << v2.x << ", "<< v2.y << ", "<< v2.z << std::endl;
//    //    }
//    
//    const Vector3 a = Vector3(x_min, y_min, z_min); //left floor front
//    const Vector3 b = Vector3(x_max, y_max, z_max); //right ceiling back
//    
//    int ax = (a.x - left_) / dx_;
//    int ay = (a.y - floor_) / dy_;
//    int az = (a.z - front_) / dz_;
//    
//    int bx = (b.x - left_) / dx_;
//    int by = (b.y - floor_) / dy_;
//    int bz = (b.z - front_) / dz_;
//    
//    //for avoid error case
//    //    if(ax>depth_t) ax--;
//    //    if(ay>depth_t) ay--;
//    //    if(az>depth_t) az--;
//    //    if(bx>depth_t) bx--;
//    //    if(by>depth_t) by--;
//    //    if(bz>depth_t) bz--;
//    
//    //1. calcurate morton
//    int morton_a = calcurate_morton(ax, ay, az);
//    int morton_b = calcurate_morton(bx, by, bz);
//    
//    //2. calcurate xor and decide belong spatial depth
//    int morton_c = morton_a^morton_b;
//    int dep_lv = 0;
//    int mor;
//    for(int i=0;i<depth_t;i++){
//        mor = morton_c >> (3 * (depth_t - i - 1));
//        if((mor & 0b111) != 0/* 0b000 */){
//            break;
//        }
//        dep_lv++;
//    }
//    
//    //3. decide belong node
//    std::cout<<"index : "<<tri->m_index<<" **********"<<std::endl;
//    Node* belong_Node = root;
//    for(int i=0;i<dep_lv;i++){
//        int n = ((morton_a >> 3*(depth_t - i - 1)) & 0b111);
//        std::cout << "child number is " << n << std::endl;
//        belong_Node = belong_Node->children[n];
//    }
//    belong_Node->ob_index.emplace_back(tri->m_index);
//    //belong_Node->ob_index.emplace_back(this->m_index);
//}
//
//int
//Octree::bitsep(int b_s, int dim){
//    int b_p = b_s;
//    int b_morton = 0;
//    for(int i=0;i<depth_t;i++){
//        b_morton += (b_p & 0b1) * std::pow(2.0, ((3 * i) + dim));
//        b_p = b_p >> 1;
//    }
//    return b_morton;
//}
//
//int
//Octree::calcurate_morton(int x, int y, int z){
//    return bitsep(x, 0)|bitsep(y, 1)|bitsep(z, 2);
//}
//
////bool
////Octree::loop_node(HitInfo& minHit, float* tMin, float* tMax,Node* node, const Ray& ray)const{
////    bool hit;
////    HitInfo tempMinHit;
////    if(!node->parent){
////        hit = false;
////        
////        //floor
////        if((*m_objects)[m_objects->size()-1]->intersect(tempMinHit, ray)){
////            hit = true;
////            if (tempMinHit.t < minHit.t){
////                minHit = tempMinHit;
////            }
////        }
////    }
////    
////    if(OctRay_aabb_intersect(node, ray)){
////        
////        //if ray intersect triangle
////        //for loop about belong of the node
////        
////        for(size_t i=0;i<node->ob_index.size();i++){
////            if ((*m_objects)[node->ob_index[i]]->intersect(tempMinHit, ray, *tMin, *tMax))
////            {
////                hit = true;
////                if (tempMinHit.t < minHit.t){
////                    minHit = tempMinHit;
////                }
////            }
////        }
////    }
////    
////    if(node->children[0]){
////        for(int i=0;i<8;i++){
////            hit = hit|OctLoop_node(minHit, tMin, tMax, node->children[i], ray);
////        }
////    }
////    
////    return hit;
////}
//
//bool
//Octree::intersect_ray_aabb(Node* node, const Ray& ray)const{
//    float tm;
//    float tM;
//    
//    vector<float> tx = intersectTime(node->left, node->right, ray, 0);
//    vector<float> ty = intersectTime(node->floor, node->ceiling, ray, 1);
//    vector<float> tz = intersectTime(node->front, node->back, ray, 2);
//    
//    tm = std::max({tx[0], ty[0], tz[0]});
//    tM = std::min({tx[1], ty[1], tz[1]});
//    
//    //std::cout<<"tM - tm = "<<tM - tm<<std::endl;
//    if(tM-tm>=0){
//        return true;
//    }
//    
//    return false;
//}
//
//vector<float>
//Octree::intersectTime(float degm, float degM, const Ray& ray, int ver)const{
//    float t_m=0.0, t_M=0.0;
//    if(ver==0){
//        if(ray.d.x > 0.0){
//            t_m = (degm - ray.o.x) / ray.d.x;
//            t_M = (degM - ray.o.x) / ray.d.x;
//        }
//        else if(ray.d.x < 0.0){
//            t_M = (degm - ray.o.x) / ray.d.x;
//            t_m = (degM - ray.o.x) / ray.d.x;
//        }
//        else{
//            t_m = -100000.0;
//            t_M = 100000.0;
//        }
//        return {t_m, t_M};
//    }
//    else if(ver==1){
//        if(ray.d.y > 0.0){
//            t_m = (degm - ray.o.y) / ray.d.y;
//            t_M = (degM - ray.o.y) / ray.d.y;
//        }
//        else if(ray.d.y < 0.0){
//            t_M = (degm - ray.o.y) / ray.d.y;
//            t_m = (degM - ray.o.y) / ray.d.y;
//        }
//        else{
//            t_m = -100000.0;
//            t_M = 100000.0;
//        }
//        return {t_m, t_M};
//    }
//    else{
//        if(ray.d.z > 0.0){
//            t_m = (degm - ray.o.z) / ray.d.z;
//            t_M = (degM - ray.o.z) / ray.d.z;
//        }
//        else if(ray.d.z < 0.0){
//            t_M = (degm - ray.o.z) / ray.d.z;
//            t_m = (degM - ray.o.z) / ray.d.z;
//        }
//        else{
//            t_m = -100000;
//            t_M = 100000;
//        }
//        return {t_m, t_M};
//    }
//}
