#ifndef MIRO_ACCEL_H_INCLUDED
#define MIRO_ACCEL_H_INCLUDED

#include "Miro.h"
#include "Object.h"
#include "Node.hpp"
#include "global_variable.hpp"

class Accel
{
public:
	void build(Objects * objs);
	bool intersect(HitInfo& result, const Ray& ray, float tMin = 0.0f, float tMax = MIRO_TMAX) const;

    //original
    void OctDevideSpatial(Node* node, float ceiling, float floor, float left, float right, float back, float front, int depth);
    void OctBuild_tree();
    void OctRegister_tri();
    bool OctLoop_node(HitInfo& minHit, float* tMin, float* tMax,Node* node, const Ray& ray) const;
    bool OctRay_aabb_intersect(Node* node, const Ray& ray) const;
    vector<float> OctIntersectTime(float degm, float degM, const Ray& ray, int ver) const;

    void build2(Objects* objs);
    
    void test();
    
    Node* root;
    
protected:
	Objects * m_objects;
};

#endif // MIRO_ACCEL_H_INCLUDED
