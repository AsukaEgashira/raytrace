#include "Triangle.h"
#include "TriangleMesh.h"
#include "Ray.h"

#include <algorithm>

#ifndef EIGEN_INCLUDED
#define EIGEN_INCLUDED
#include "Eigen/Dense" // for calculate matrix
#include "global_variable.hpp"
#endif

Triangle::Triangle(TriangleMesh * m, unsigned int i) :
	m_mesh(m), m_index(i)
{
}


Triangle::~Triangle()
{
}


void
Triangle::renderGL()
{
	TriangleMesh::TupleI3 ti3 = m_mesh->vIndices()[m_index];
	const Vector3 & v0 = m_mesh->vertices()[ti3.x]; //vertex a of triangle
	const Vector3 & v1 = m_mesh->vertices()[ti3.y]; //vertex b of triangle
	const Vector3 & v2 = m_mesh->vertices()[ti3.z]; //vertex c of triangle

	glBegin(GL_TRIANGLES);
		glVertex3f(v0.x, v0.y, v0.z);
		glVertex3f(v1.x, v1.y, v1.z);
		glVertex3f(v2.x, v2.y, v2.z);
	glEnd();
}



bool
Triangle::intersect(HitInfo& result, const Ray& r,float tMin, float tMax)
{
	//write yourself
	TriangleMesh::TupleI3 ti3 = m_mesh->vIndices()[m_index];
	const Vector3 & v0 = m_mesh->vertices()[ti3.x]; //vertex a of triangle
	const Vector3 & v1 = m_mesh->vertices()[ti3.y]; //vertex b of triangle
	const Vector3 & v2 = m_mesh->vertices()[ti3.z]; //vertex c of triangle

	Eigen::Matrix3f A;
	Eigen::Matrix3f A_;
	Eigen::Vector3f b;
    //Eigen::Vector3f c;
    A << v0.x-v1.x,v0.x-v2.x,r.d.x,
		v0.y-v1.y,v0.y-v2.y,r.d.y,
		v0.z-v1.z,v0.z-v2.z,r.d.z;

	b << v0.x-r.o.x,
		v0.y-r.o.y,
		v0.z-r.o.z;

	//static double time_compute_inverse=0;
	//write here timer set.

	A_ = A.inverse();

	//Write here timer stop
	// add time to time_compute_inverse.

	Eigen::Vector3f c = A_*b;

    //Eigen::Vector3f c = solve(A, b);
    //Eigen::Vector3f c = A.fullPivLu().solve(b);
    //Eigen::Vector3f c = A.partialPivLu().solve(b);
    //Eigen::PartialPivLU<Eigen::Matrix3f> lu(A);
    //c = lu.solve(b);
    
    
    
	const float t = c(2,0);
	if(t>=tMax || t<=tMin) return false;
	const float gamma = c(1,0);
	if(gamma >= 1.0 || gamma <= 0.0) return false;
	const float beta = c(0,0);
	if(beta >= 1.0 || beta <= 0.0) return false;
	const float alpha = 1.0 - beta - gamma;
	if(alpha >= 1.0 || alpha <= 0.0) return false;
	result.t = t;
	result.P = r.o + result.t*r.d;
	result.N = cross(v0-v1, v2-v1);
	if(dot(result.N, r.o) < 0.0) result.N = -result.N;
	result.N.normalize();
	result.material = this->m_material;

	return true;
	//end
	//return false;
}

int
Triangle::bitsep(int b_s, int dim){
    int b_p = b_s;
    int b_morton = 0;
    for(int i=0;i<depth_t;i++){
        b_morton += (b_p & 0b1) * std::pow(2.0, ((3 * i) + dim));
        b_p = b_p >> 1;
    }
    return b_morton;
}

int
Triangle::calcurate_morton(int x, int y, int z){
    return bitsep(x, 0)|bitsep(y, 1)|bitsep(z, 2);
}


void
Triangle::belong(Node* root){
    //0. position aabb
    TriangleMesh::TupleI3 ti3 = m_mesh->vIndices()[m_index];
    const Vector3 & v0 = m_mesh->vertices()[ti3.x]; //vertex a of triangle
    const Vector3 & v1 = m_mesh->vertices()[ti3.y]; //vertex b of triangle
    const Vector3 & v2 = m_mesh->vertices()[ti3.z]; //vertex c of triangle
    float x_max = std::max({v0.x, v1.x, v2.x});
    float x_min = std::min({v0.x, v1.x, v2.x});
    float y_max = std::max({v0.y, v1.y, v2.y});
    float y_min = std::min({v0.y, v1.y, v2.y});
    float z_max = std::max({v0.z, v1.z, v2.z});
    float z_min = std::min({v0.z, v1.z, v2.z});
    
//    if(this->m_index % 30 ==0){
//        std::cout<< this->m_index << " : " << std::endl;
//        std::cout << v0.x << ", "<< v0.y << ", "<< v0.z << std::endl;
//        std::cout << v1.x << ", "<< v1.y << ", "<< v1.z << std::endl;
//        std::cout << v2.x << ", "<< v2.y << ", "<< v2.z << std::endl;
//    }
    
    const Vector3 a = Vector3(x_min, y_min, z_min); //left floor front
    const Vector3 b = Vector3(x_max, y_max, z_max); //right ceiling back
    
    int ax = (a.x - left_) / dx_;
    int ay = (a.y - floor_) / dy_;
    int az = (a.z - front_) / dz_;
    
    int bx = (b.x - left_) / dx_;
    int by = (b.y - floor_) / dy_;
    int bz = (b.z - front_) / dz_;
    
    //for avoid error case
//    if(ax>depth_t) ax--;
//    if(ay>depth_t) ay--;
//    if(az>depth_t) az--;
//    if(bx>depth_t) bx--;
//    if(by>depth_t) by--;
//    if(bz>depth_t) bz--;
    
    //1. calcurate morton
    int morton_a = calcurate_morton(ax, ay, az);
    int morton_b = calcurate_morton(bx, by, bz);
    
    //2. calcurate xor and decide belong spatial depth
    int morton_c = morton_a^morton_b;
    int dep_lv = 0;
    int mor;
    for(int i=0;i<depth_t;i++){
        mor = morton_c >> (3 * (depth_t - i - 1));
        if((mor & 0b111) != 0/* 0b000 */){
            break;
        }
        dep_lv++;
    }
    
    //3. decide belong node
    std::cout<<"index : "<<m_index<<" **********"<<std::endl;
    Node* belong_Node = root;
    for(int i=0;i<dep_lv;i++){
        int n = ((morton_a >> 3*(depth_t - i - 1)) & 0b111);
        //std::cout << "child number is " << n << std::endl;
        belong_Node = belong_Node->children[n];
    }
    belong_Node->ob_index.emplace_back(this->oct_index);
    //belong_Node->ob_index.emplace_back(this->m_index);
    
}
