//
//  global_variable.cpp
//  basic_tracer
//
//  Created by asuka egashira on 2017/09/07.
//  Copyright © 2017年 asuka egashira. All rights reserved.
//

#include "global_variable.hpp"

float left_;
float right_;
float floor_;
float ceil_;
float front_;
float back_;

obj_kind obj_t = bunny20;
tree_kind tree = octree; //0:thorough , 1:octree, 2:kdtree
int depth_t = 8;

float dx_;
float dy_;
float dz_;
