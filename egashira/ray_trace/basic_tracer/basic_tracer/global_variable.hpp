//
//  global_variable.hpp
//  basic_tracer
//
//  Created by asuka egashira on 2017/09/07.
//  Copyright © 2017年 asuka egashira. All rights reserved.
//

#ifndef global_variable_hpp
#define global_variable_hpp

#include <stdio.h>

typedef enum{
    teapot,
    bunny,
    bunny20,
    sponza
}obj_kind;

typedef enum{
    thorough_search,
    octree,
    kdtree
}tree_kind;

//kind of obj
extern obj_kind obj_t;

//root box
extern float left_;
extern float right_;
extern float floor_;
extern float ceil_;
extern float front_;
extern float back_;

//depth of tree
extern tree_kind tree;
extern int depth_t;

//per x , y, z
extern float dx_;
extern float dy_;
extern float dz_;

#endif /* global_variable_hpp */
