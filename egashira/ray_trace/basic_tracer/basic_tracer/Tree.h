//
//  Tree.h
//  basic_tracer
//
//  Created by asuka egashira on 2017/09/11.
//  Copyright © 2017年 asuka egashira. All rights reserved.
//

#ifndef Tree_h
#define Tree_h

#include "Node.hpp"
#include "Triangle.h"
#include "Miro.h"

class Tree{
public:
    Tree(){}
    virtual ~Tree(){}
    
    virtual void make_tree(Node* node){}
    
    virtual void register_mesh(Triangle* tri){}
    
    virtual bool loop_node(HitInfo& minHit, float* tMin, float* tMax,Node* node, const Ray& ray)const{return false;}
    
    Node* root;
};

#endif /* Tree_h */
