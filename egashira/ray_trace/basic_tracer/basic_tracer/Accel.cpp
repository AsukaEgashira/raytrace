#include "Accel.h"
#include "Ray.h"
#include "Console.h"
//#include "Node.hpp"

#include <algorithm>

#include <Eigen/Dense>
#include <igl/readOBJ.h>

#include "Triangle.h"
#include "TriangleMesh.h"
#include "global_variable.hpp"
#include <string>

Eigen::MatrixXd V;
Eigen::MatrixXi F;

void
Accel::build(Objects * objs)
{
	// construct the bounding volume hierarchy
	m_objects = objs;
    OctBuild_tree();
    OctRegister_tri();
    //(*m_objects)[i]-
    //test();
}

void
Accel::test(){
    Ray test_ray = Ray({10.0,10.0,10.0}, {-0.5518, -0.4904, -0.6744});
    bool test_aabb=OctRay_aabb_intersect(root->children[5], test_ray);
    std::cout<<"can we detect ray and aabb collision? : "<<test_aabb<<std::endl;
    std::cout<<"";
    
    std::cout<<"count triagnle num"<<std::endl<<root->ob_index.size()<<", ";
    for(int i=0;i<8;i++){
        std::cout<<root->children[i]->ob_index.size()<<", ";
    }
     std::cout<<"";
     std::cout<<"";
}

vector<float>
Accel::OctIntersectTime(float degm, float degM, const Ray& ray, int ver) const{
    float t_m=0.0, t_M=0.0;
    if(ver==0){
        if(ray.d.x > 0.0){
            t_m = (degm - ray.o.x) / ray.d.x;
            t_M = (degM - ray.o.x) / ray.d.x;
        }
        else if(ray.d.x < 0.0){
            t_M = (degm - ray.o.x) / ray.d.x;
            t_m = (degM - ray.o.x) / ray.d.x;
        }
        else{
            t_m = -100000.0;
            t_M = 100000.0;
        }
        return {t_m, t_M};
    }
    else if(ver==1){
        if(ray.d.y > 0.0){
            t_m = (degm - ray.o.y) / ray.d.y;
            t_M = (degM - ray.o.y) / ray.d.y;
        }
        else if(ray.d.y < 0.0){
            t_M = (degm - ray.o.y) / ray.d.y;
            t_m = (degM - ray.o.y) / ray.d.y;
        }
        else{
            t_m = -100000.0;
            t_M = 100000.0;
        }
        return {t_m, t_M};
    }
    else{
        if(ray.d.z > 0.0){
            t_m = (degm - ray.o.z) / ray.d.z;
            t_M = (degM - ray.o.z) / ray.d.z;
        }
        else if(ray.d.z < 0.0){
            t_M = (degm - ray.o.z) / ray.d.z;
            t_m = (degM - ray.o.z) / ray.d.z;
        }
        else{
            t_m = -100000;
            t_M = 100000;
        }
        return {t_m, t_M};
    }
}

bool
Accel::OctRay_aabb_intersect(Node* node, const Ray& ray) const{
    float tm;
    float tM;

    vector<float> tx = OctIntersectTime(node->left, node->right, ray, 0);
    vector<float> ty = OctIntersectTime(node->floor, node->ceiling, ray, 1);
    vector<float> tz = OctIntersectTime(node->front, node->back, ray, 2);
    
    tm = std::max({tx[0], ty[0], tz[0]});
    tM = std::min({tx[1], ty[1], tz[1]});
    
    //std::cout<<"tM - tm = "<<tM - tm<<std::endl;
    if(tM-tm>=0){
        return true;
    }
    return false;
}

bool
Accel::OctLoop_node(HitInfo& minHit, float* tMin, float* tMax, Node* node, const Ray& ray) const{
    bool hit;
    HitInfo tempMinHit;
    if(!node->parent){
        hit = false;
        
        //floor
        if(obj_t != 3){
            if((*m_objects)[m_objects->size()-1]->intersect(tempMinHit, ray)){
                hit = true;
                if (tempMinHit.t < minHit.t){
                    minHit = tempMinHit;
                }
            }
        }
    }
    
    bool aabb_intersect = OctRay_aabb_intersect(node, ray);
    
    if(aabb_intersect){
    
        //if ray intersect triangle
        //for loop about belong of the node
        
        for(size_t i=0;i<node->ob_index.size();i++){
            if ((*m_objects)[node->ob_index[i]]->intersect(tempMinHit, ray, *tMin, *tMax))
            {
                hit = true;
                if (tempMinHit.t < minHit.t){
                    minHit = tempMinHit;
                }
            }
        }
    }
    
    if(node->children[0]&&aabb_intersect){
        for(int i=0;i<8;i++){
            hit = hit|OctLoop_node(minHit, tMin, tMax, node->children[i], ray);
        }
    }
    
    return hit;
}

bool
Accel::intersect(HitInfo& minHit, const Ray& ray, float tMin, float tMax) const
{
	// Here you would need to traverse the acceleration data structure to perform ray-intersection
	// acceleration. For now we just intersect every object.
    
	bool hit = false;
	HitInfo tempMinHit;
	minHit.t = MIRO_TMAX;
    
    if(tree==1){
        hit = OctLoop_node(minHit, &tMin, &tMax, root, ray);
    }
    else{
        for (size_t i = 0; i < m_objects->size(); ++i)
        {
            if ((*m_objects)[i]->intersect(tempMinHit, ray, tMin, tMax))
            {
                hit = true;
                if (tempMinHit.t < minHit.t)
                    minHit = tempMinHit;
            }
        }
        std::cout << std::endl;
    }

	return hit;
}

void
Accel::OctDevideSpatial(Node* node, float ceil, float floor, float left, float right, float back, float front, int depth){
    if(depth > depth_t){
        for(int i=0; i<8; i++){
            node->children[i] = nullptr;
        }
        return;
    }
    
    float mid_h = (ceil + floor)/2, mid_w = (right + left)/2, mid_d = (back + front)/2;
    
    float x_max[8] = {mid_w, right, mid_w, right, mid_w, right, mid_w, right};
    float x_min[8] = {left, mid_w, left, mid_w, left, mid_w, left, mid_w};
    float y_max[8] = {mid_h, mid_h, ceil, ceil, mid_h, mid_h, ceil, ceil};
    float y_min[8] = {floor, floor, mid_h, mid_h, floor, floor, mid_h, mid_h};
    float z_max[8] = {mid_d, mid_d, mid_d, mid_d, back, back, back, back};
    float z_min[8] = {front, front, front, front, mid_d, mid_d, mid_d, mid_d};
    
    for(int i=0;i<8;i++){
        node->children[i] = new Node(y_max[i], y_min[i], x_min[i], x_max[i], z_max[i], z_min[i], depth);
        node->children[i]->parent = node;
        
//        std::cout<<"children " << i << " : "<<std::endl;
//        std::cout<<node->children[i]->left<<", "<<node->children[i]->right<<", "<<node->children[i]->floor<<", "
//        <<node->children[i]->ceiling<<", "<<node->children[i]->front<<", "<<node->children[i]->back<<std::endl;
        
        int c_dep = depth + 1;
        OctDevideSpatial(node->children[i], y_max[i], y_min[i], x_min[i], x_max[i], z_max[i], z_min[i], c_dep);
    }
}

void
Accel::OctBuild_tree(){
    //get max and min of width, height, depth
    //bool canread =
    if(obj_t == 0){
        igl::readOBJ("/Users/asuka/raytrace/egashira/ray_trace/data" "/teapot.obj", V, F);
    }
    else if (obj_t == 3){
        igl::readOBJ("/Users/asuka/raytrace/egashira/ray_trace/data" "/sponza.obj", V, F);
    }
    else if(obj_t == 1||obj_t == 2) {
        igl::readOBJ("/Users/asuka/raytrace/egashira/ray_trace/data" "/bunny.obj", V, F);
    }


    //std::cout << "Can read? " << canread << std::endl;
    
    Eigen::Vector3d m = V.colwise().minCoeff();
    Eigen::Vector3d M = V.colwise().maxCoeff();
    
    left_ = m(0);
    right_ = M(0);
    floor_ = m(1);
    ceil_ = M(1);
    front_ = m(2);
    back_ = M(2);
    
    if(obj_t==2){
        left_ -= 8.0;
        floor_ -= 8.0;
        front_ -= 8.0;
        right_ += 8.0;
        ceil_ += 8.0;
        back_ += 8.0;
    }
    
    dx_ = (right_ - left_) / std::pow(2.0, depth_t);
    dy_ = (ceil_ - floor_) / std::pow(2.0, depth_t);
    dz_ = (back_ - front_) / std::pow(2.0, depth_t);
    
    root = new Node(ceil_, floor_, left_, right_, back_, front_, 0);
    root->parent = nullptr;
    
    OctDevideSpatial(root, ceil_, floor_, left_, right_, back_, front_, 1);
}

void
Accel::OctRegister_tri(){
    for (size_t i = 0; i < m_objects->size() - 1; ++i)
    {
        (*m_objects)[i]->belong(root);
    }
}
