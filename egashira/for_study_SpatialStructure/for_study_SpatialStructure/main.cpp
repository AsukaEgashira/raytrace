#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <vector>
# include <random>
#include <iostream>
#include <algorithm>
#include <chrono>
#include <fstream>
#include <string>

#include "global_variables.hpp"
#include "Node.hpp"
#include "t_particle.hpp"

using std::vector;
using std::fabs;

float ratio;
int width, height;

const float radius_max=uniform_radius?particle_radius: 0.02f;
const float radius_min=uniform_radius?particle_radius: 0.02f;
const GLfloat external_force_coef=uniform_density?0.01f:1.f;

vector<t_particle> particles;
int depth_t = 4; //min: 1 ~ max: infinity
Node* root;
int max_cr = std::pow(2.0, depth_t);
float d_x = (right_ - left_) / max_cr;
float d_y = (floor_ - ceil_) / max_cr;
//vector<t_particle*> candidate_list;

bool tree_swith = true;


inline void devideSpatial(Node* node, float ceiling, float floor, float left, float right, int depth){
    if(depth > depth_t) {
        for(int j=0;j<4;j++){
            node->children[j] = nullptr;
        }
        return;
    }
    
    float mid_h = (ceiling + floor) / 2, mid_l = (left + right) / 2;
    float y_max[4] = {ceiling, ceiling, mid_h, mid_h}; //ceiling
    float y_min[4] = {mid_h, mid_h, floor, floor}; //floor
    float x_max[4] = {mid_l, right, mid_l, right}; //right
    float x_min[4] = {left, mid_l, left, mid_l}; //left
    
    for(int i=0;i<4;i++){
        node->children[i] = new Node(y_max[i], y_min[i], x_min[i], x_max[i], depth);
        node->children[i]->parent = node;
        
        int c_dep = depth + 1;
        devideSpatial(node->children[i], y_max[i], y_min[i], x_min[i], x_max[i], c_dep);
    }
}

void build_quad_tree(){
    int current_dep = 0;
    //////////////////////////////////////////////////////////
    //attention! ceil_ : floor height, floor_ : ceiling height
    //////////////////////////////////////////////////////////
    root=new Node(floor_, ceil_, left_, right_, current_dep);
    root->parent = nullptr;
    current_dep++;
//    std::cout << "***********************" << std::endl;
//    std::cout << "* start to build tree *" << std::endl;
//    std::cout << "***********************" << std::endl;
    devideSpatial(root, floor_, ceil_, left_, right_, current_dep);
}

bool Isleaf(Node* node){
    if(node->children[0] == nullptr){
        return true;
    }
    return false;
}

void delete_children(Node* node){
    for(int i=0; i<4; i++){
        delete node->children[i];
    }
}

void detect_min_parent(Node* node){
    if(Isleaf(node->children[0])){
        delete_children(node);
        return;
    }
    else{
        for(int j=0;j<4;j++){
            detect_min_parent(node->children[j]);
        }
    }
}

void delete_quad_tree(){
    detect_min_parent(root);
}

int bitsep(int b_se, int even){
    int b_p = b_se;
    int b_morton = 0;
    for(int i=0;i<depth_t;i++){
        b_morton += (b_p & 0b1) * std::pow(2.0, ((2 * i) + even));
        b_p = b_p >> 1;
    }
    return b_morton;
}

uint calculate_morton(int col_, int row_){
    
    return bitsep(col_, 0)|bitsep(row_, 1);
}

void belong(t_particle* particle){
    PVector a = {particle->position.pvec[0] + particle->radius, particle->position.pvec[1] + particle->radius};
    PVector b = {particle->position.pvec[0] - particle->radius, particle->position.pvec[1] - particle->radius};
    
    int a_x = ( - left_ + a.pvec[0]) / d_x;
    int a_y = (floor_ - a.pvec[1]) / d_y;
    int b_x = ( - left_ + b.pvec[0]) / d_x;
    int b_y = (floor_ - b.pvec[1]) / d_y;
    
    //1. calculate morton
    int morton_a = calculate_morton(a_x, a_y);
    int morton_b = calculate_morton(b_x, b_y);
    
    //2. calculate a xor b and decide belong spatial level
    int morton_c = morton_a^morton_b;
    int dep_lv = 0;
    int mor;
    for(int i=0;i<depth_t;i++){
        mor = morton_c >> (2 * (depth_t - i - 1));
        if((mor & 0b11) != 00){
            break;
        }
        dep_lv++;
    }
    
    //3. decide belong spatial
    Node* belong_Node = root;
    for(int i=0;i<dep_lv;i++){
        int n = ((morton_a >> 2*(depth_t - i - 1)) & 0b11);
        //std::cout << "child number is " << n << std::endl;
        belong_Node = belong_Node->children[n];
    }
    //std::cout << "***************************" << std::endl;
    belong_Node->belong.emplace_back(particle);
    belong_Node->candidate.emplace_back(particle);
    particle->depth = dep_lv;
}

void add_candidate(Node* node, vector<t_particle*> c_list){
    c_list.reserve(c_list.size() + node->belong.size());
    c_list.insert(c_list.end(), node->belong.begin(), node->belong.end());
    for(auto& j : node->belong){
        j->candidate.reserve(j->candidate.size() + c_list.size());
        j->candidate.insert(j->candidate.end(), c_list.begin(), c_list.end());
    }
    if(Isleaf(node)){
        return;
    }
    for(int i=0;i<4;i++){
        add_candidate(node->children[i], c_list);
        node->children[i]->belong.clear();//initialize
    }
    node->belong.clear();//initialize
}

void add_candidate2(Node* node){
    //node->candidate.reserve(node->candidate.size() + node->belong.size());
    //node->candidate.insert(node->candidate.end(), node->belong.begin(), node->belong.end());
    if(node->parent){
        node->candidate.reserve(node->candidate.size() + node->parent->candidate.size());
        node->candidate.insert(node->candidate.end(), node->parent->candidate.begin(), node->parent->candidate.end());
    }
    if(Isleaf(node)){
        return;
    }
    for(int i=0;i<4;i++){
        add_candidate2(node->children[i]);
    }
}

inline void call_collision(Node* node){
    for(int i=0;i<node->belong.size();i++){
        for(int j=i+1;j<node->candidate.size();j++){
            node->belong[i]->collide(*(node->candidate[j]));
        }
    }
    node->belong.clear();
    node->candidate.clear();
    if(Isleaf(node)) return;
    for(int i=0;i<4;i++){
        call_collision(node->children[i]);
    }
}

inline void handleCollision(){ //let's  write spatial structure
    
//    std::cout << "candidate : ";
//    std::chrono::system_clock::time_point start3 = std::chrono::system_clock::now();
      //add_candidate(root, candidate_list);
//    std::chrono::system_clock::time_point end3 = std::chrono::system_clock::now();
//    double time3 = std::chrono::duration_cast<std::chrono::nanoseconds>(end3-start3).count();
//    std::cout << time3 << std::endl;
    
//    for(int i=0;i!=particles.size();++i){
//        for(int j=0;j!=particles[i].candidate.size();j++){
//            if((&particles[i] != particles[i].candidate[j])&&(&particles[i] < particles[i].candidate[j])){
//                particles[i].collide(*particles[i].candidate[j]);
//            }
//            else if(particles[i].depth > particles[i].candidate[j]->depth){
//                particles[i].collide(*particles[i].candidate[j]);
//            }
//        }
//    }
    
    if(tree_swith){
        add_candidate2(root);
        call_collision(root);
    }
    else{
        for(int i=0;i!=particles.size();++i){
            for(int j=i+1;j!=particles.size();++j){
                
                particles[i].collide(particles[j]);
                
            }
        }
    }
}


inline void drawFrame(){
    static const GLfloat vtx3[] = {
        left_, ceil_,
        left_, floor_,
        right_, floor_,
        right_, ceil_,
    };
    
    glVertexPointer(2, GL_FLOAT, 0, vtx3);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    
    glEnable(GL_MULTISAMPLE);
    glMatrixMode(GL_MODELVIEW);
    
    //glDrawArrays has to be sandwiched by glEnableCliantState and glDisableCliantState.
    glEnableClientState(GL_VERTEX_ARRAY);
    
    glPushMatrix();
    glDrawArrays(GL_QUADS, 0, 4);
    glPopMatrix();
    
    glDisableClientState(GL_VERTEX_ARRAY);
}

inline void update(){
    
//    std::cout << "belong : ";
//    std::chrono::system_clock::time_point start2 = std::chrono::system_clock::now();
    for( auto it=particles.begin();it!=particles.end();it++){
        it->update();
        if(tree_swith){
            belong(&*it);
        }
    }
//    std::chrono::system_clock::time_point end2 = std::chrono::system_clock::now();
//    double time2 = std::chrono::duration_cast<std::chrono::nanoseconds>(end2-start2).count();
//    std::cout << time2 << std::endl;
    
    handleCollision();
    //candidate_list.clear();
}

inline void drawParticles(){
    glColor3f(0.f, 0.f, 1.f);
    float sizePx;

    glEnable(GL_POINT_SMOOTH);
    if(uniform_radius){
        sizePx=(height*particle_radius/(1.f+1.f)*2.f);
        glPointSize( sizePx );
        
        glBegin(GL_POINTS);
        for( auto &particle:particles){
            
            particle.draw();
            
        }
        glEnd();
        
    }else{
        
    }
    for( auto &particle:particles){
        sizePx=(height*particle.radius/(1.f+1.f)*2.f);
        
        glPointSize( sizePx );
        
        glBegin(GL_POINTS);
        
        particle.draw();
        
        
        glEnd();
    }
}


void generate_particles(){
    
    //Generate particles.
    //Initial positions and velocities are given by Mersenne twister.
    
    static std::random_device rd;
    //Random number generater using Mersenne twister.
    static std::mt19937 engine(rd()) ;
    
    //Generate positions and velocities under uniform distribution.
    std::uniform_real_distribution<GLfloat> distribution_x( left_,  right_) ;
    std::uniform_real_distribution<GLfloat> distribution_y( ceil_,  floor_) ;
    std::uniform_real_distribution<GLfloat> distribution_vx( v_min,  v_max) ;
    std::uniform_real_distribution<GLfloat> distribution_vy( v_min,  v_max) ;
    
    std::uniform_real_distribution<GLfloat> distribution_r( radius_min,  radius_max) ;
    
    for(int i=0;i<num_particle;i++){
        float x=distribution_x(engine);
        float y=distribution_y(engine);
        float vx=distribution_vx(engine);
        float vy=distribution_vy(engine);
        
        float r=distribution_r(engine);
        
        float mass=uniform_density?1.f*r*r:1.f;
        
        particles.push_back((t_particle(PVector(x, y), PVector(vx, vy), mass,r)));
    }
}

inline void addForce(PVector external_force){
    for(auto it=particles.begin();it!=particles.end();it++){
        it->addForce(external_force);
    }
}

const float goToAndFrom_speed=0.1f;//0.1f;
inline void goToPoint(PVector point){
    for(auto it=particles.begin();it!=particles.end();it++){
        PVector direction=point-it->position;
        
        //it->force=it->force+goToAndFrom_speed*direction;
        it->velocity+=goToAndFrom_speed*direction;
    }
}

inline void goFromPoint(PVector point){
    for(auto it=particles.begin();it!=particles.end();it++){
        PVector direction=point-it->position;
        float distance=direction.norm();
        
        //        it->force=it->force-goToAndFrom_speed*direction;
        it->velocity-=goToAndFrom_speed*direction/(distance*distance);
        
        
    }
}

static void clear(){
    particles.clear();
}

void render (GLFWwindow * window){
    
    
    
    glfwGetFramebufferSize(window, &width, &height);
    ratio = width / (float)height;
    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT);
    
    //ウインドウの引き伸ばしに不変にする。描画範囲の設定
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    
    //If setting up, glOrtho(0.0f, width, 0.0f, height, -1.0f, 1.0f);
    //then the window size coincides with the inner scale.
    glOrtho(-ratio, ratio, -1.f, 1.f, 1.f, -1.f);
    
    
    
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glPushMatrix();
    drawFrame();
    glPopMatrix();
    
    glPushMatrix();
    glRotatef(180, 1, 0, 0);
    drawParticles();
    glPopMatrix();
    
    
    glfwSwapBuffers(window);
    glfwPollEvents();
}

static void error_callback(int error, const char* description)
{
    fputs(description, stderr);
}



static void key_callback(GLFWwindow* window, int key, int scancode, int action,
                         int mods)
{
    
    if ( key==GLFW_KEY_A  &&action==GLFW_PRESS){
        generate_particles();
    }
    
    
    if ( (key==GLFW_KEY_R or key==GLFW_KEY_0)  &&action==GLFW_PRESS){
        clear();
        generate_particles();
    }
    
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
    PVector ex_force(0,0);
    if (key==GLFW_KEY_RIGHT ){
        ex_force+=PVector(external_force_coef,0);
    }
    if (key==GLFW_KEY_LEFT ){
        ex_force+=PVector(-external_force_coef,0);
    }
    if (key==GLFW_KEY_DOWN ){
        ex_force+=PVector(0,external_force_coef);
    }
    if (key==GLFW_KEY_UP ){
        ex_force+=PVector(0,-external_force_coef);
    }
    
    if(action==GLFW_PRESS){
        addForce(ex_force);
    }else if (action==GLFW_RELEASE){
        addForce(-ex_force);
    }
}

static void mouse_input(GLFWwindow*window){
    
    if( glfwGetMouseButton(window,GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS ) {
        double mousex, mousey;
        glfwGetCursorPos(window,&mousex, &mousey);
        
        //Translate the mouse coordinate (mousex,mousey) in [0,width]*[0,height]
        //to target position in [-1,1]*[-,1,1].
        int width, height;
        glfwGetFramebufferSize(window, &width, &height);
        GLfloat x,y;
        GLfloat W=width/2,H=height/2;
        x=(mousex-W)/H;
        y=(mousey/height-0.5)*2;
        PVector target_position(x,y);
        goToPoint(target_position);
        
    }
    
    if( glfwGetMouseButton(window,GLFW_MOUSE_BUTTON_LEFT) == GLFW_RELEASE ) {
        
    }
    
    
    
    if( glfwGetMouseButton(window,GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS ) {
        double mousex, mousey;
        glfwGetCursorPos(window,&mousex, &mousey);
        
        //Translate the mouse coordinate (mousex,mousey) in [0,width]*[0,height]
        //to target position in [-1,1]*[-,1,1].
        int width, height;
        glfwGetFramebufferSize(window, &width, &height);
        GLfloat x,y;
        GLfloat W=width/2,H=height/2;
        x=(mousex-W)/H;
        y=(mousey/height-0.5)*2;
        PVector target_position(x,y);
        goFromPoint(target_position);
        
    }
    if( glfwGetMouseButton(window,GLFW_MOUSE_BUTTON_RIGHT) == GLFW_RELEASE ) {
        
    }
    
}

void test(){
    for(auto i = root->belong.begin(), j=i; i < root->belong.end(); i++){
        i++;
        if(i==j){
            std::cout << "belong of node not clear" << std::endl;
        }
    }
    for(auto i = particles[0].candidate.begin(), j=i; i < particles[0].candidate.end(); i++){
        i++;
        if(i==j){
            std::cout << "candidate of particle not clear" << std::endl;
        }
    }
    //*********
    //test tree
    //*********
    /*
     std::cout << "test aabb1\n" << root->children[0]->ceiling << "\n" <<
     root->children[0]->floor << "\n" <<
     root->children[0]->left << "\n" <<
     root->children[0]->right << std::endl;
     std::cout << "test aabb2\n" << root->children[0]->children[0]->ceiling << "\n" <<
     root->children[0]->children[0]->floor << "\n" <<
     root->children[0]->children[0]->left << "\n" <<
     root->children[0]->children[0]->right << std::endl;
     std::cout << "test aabb3\n" << root->children[0]->children[0]->children[0]->ceiling << "\n" <<
     root->children[0]->children[0]->children[0]->floor << "\n" <<
     root->children[0]->children[0]->children[0]->left << "\n" <<
     root->children[0]->children[0]->children[0]->right << std::endl;
     */
    
    
    //************
    //test morton
    //************
    //std::cout << "test calculate morton : " << calculate_morton(6, 6) << std::endl;
    
    //************
    //test belong
    //************
    /*
     t_particle* test_particle = new t_particle({-0.8, 0.8}, {0.0, 0.0}, 0.0);
     test_particle->radius = 0.01;
     int max_cr = std::pow(2.0, depth_t);
     float d_x = (right_ - left_) / max_cr;
     float d_y = (floor_ - ceil_) / max_cr;
     belong(test_particle, d_x, d_y, max_cr, 0);
     if(root->children[3]->sub_par.empty()){
     std::cout << "non non" << std::endl;
     }
     std::cout << "test_belong\n" << root->children[0]->children[0]->children[0]->sub_par[0] << std::endl;
     */
    
    //***************
    //test candidate
    //***************
    /*
     vector<t_particle*> test_particle(6);
     test_particle[0] =  new t_particle({-0.8, 0.8}, {0.0, 0.0}, 0.0, 0.05);
     test_particle[1] =  new t_particle({-0.42, 0.55}, {0.0, 0.0}, 0.0, 0.05);
     test_particle[2] =  new t_particle({-0.3, 0.3}, {0.0, 0.0}, 0.0, 0.05);
     test_particle[3] =  new t_particle({0.0, 0.7}, {0.0, 0.0}, 0.0, 0.05);
     test_particle[4] =  new t_particle({0.3, -0.3}, {0.0, 0.0}, 0.0, 0.05);
     test_particle[5] =  new t_particle({0.8, -0.8}, {0.0, 0.0}, 0.0, 0.05);
     
     int max_cr = std::pow(2.0, depth_t);
     float d_x = (right_ - left_) / max_cr;
     float d_y = (floor_ - ceil_) / max_cr;
     for(int i=0;i!=test_particle.size();++i){
     belong(test_particle[i], &d_x, &d_y);
     }
     
     std::cout << root->children[0]->children[0]->belong[0]->position[0] << ","
     << root->children[0]->children[0]->belong[0]->position[1] << std::endl;
     std::cout << root->children[0]->belong[0]->position[0] << ","
     << root->children[0]->belong[0]->position[1] << std::endl;
     std::cout << root->children[0]->children[3]->belong[0]->position[0] << ","
     << root->children[0]->children[3]->belong[0]->position[1] << std::endl;
     
     vector<t_particle*> list;
     add_candidate(root, list);
     
     std::cout << "particle4 can collide..." <<std::endl;
     for(auto i : test_particle[3]->candidate){
     std::cout<<i->position[0]<<", "<<i->position[1]<<std::endl;
     }
     
     puts("");
     */
}

int main(void)
{
    GLFWwindow* window;
    glfwSetErrorCallback(error_callback);
    if (!glfwInit())
        exit(EXIT_FAILURE);
    window = glfwCreateWindow(640, 480, "Simple example", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);
    
    glfwSetKeyCallback(window, key_callback);
    
    
    //Timer setting
    double FPS = 60;
    double currentTime, lastTime, elapsedTime;
    currentTime = lastTime = elapsedTime = 0.0;
    glfwSetTime(0.0); //Initialize timer.
    
    dt=1/FPS;
    
    //get time
    std::string filename = "time.txt";
    int t=0, count=0;
    std::chrono::system_clock::time_point  start, end; // type can be auto
    start = std::chrono::system_clock::now();
    
    generate_particles();
    
//    std::chrono::system_clock::time_point start1 = std::chrono::system_clock::now();
    if(tree_swith){
    build_quad_tree();
    }
//    std::chrono::system_clock::time_point end1 = std::chrono::system_clock::now();
//    double time1 = std::chrono::duration_cast<std::chrono::nanoseconds>(end1-start1).count();
//    std::cout << "build tree : ";
//    std::cout << time1 << std::endl;
    while (!glfwWindowShouldClose(window))
    {
        
        glClearColor(0.3f, .3f, 0.3f, 0.f);
        currentTime = glfwGetTime();
        elapsedTime = currentTime - lastTime;
        
        if (elapsedTime > 1.0/FPS) {
            
            mouse_input(window);
            
            update();
            
            render(window);
            
            lastTime = glfwGetTime();
            
            //measure time
            t++;
            if(t==1000){
                end = std::chrono::system_clock::now();
                double elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
                std::ofstream file;
                file.open(filename, std::ios::app);
                {
                    file<< "depth = " << depth_t<<std::endl;
                    file<<elapsed<<"[ms]"<<std::endl;
                    file<<"**********************"<<std::endl;
                }
                count++;
                if(count==1){
                    glfwSetWindowShouldClose(window, GL_TRUE);
                }
                t=0;
                clear();
                if(tree_swith){
                    delete_quad_tree();
                }
                start = std::chrono::system_clock::now();//next
                
                generate_particles();
                build_quad_tree();
            }
        }
        
    }
    if(tree_swith){
        delete_quad_tree();
    }
    glfwDestroyWindow(window);
    glfwTerminate();
    exit(EXIT_SUCCESS);
}
