//
//  t_particle.cpp
//  for_study_SpatialStructure
//
//  Created by asuka egashira on 2017/08/29.
//  Copyright © 2017年 asuka egashira. All rights reserved.
//

#include "global_variables.hpp"
#include "t_particle.hpp"

t_particle::t_particle(PVector position, PVector velocity, float mass,float radius):position(position),velocity(velocity),mass(mass),force(gravity*mass),original_force(force),radius(radius) {
}

t_particle::~t_particle(){
}

 void
t_particle::resetForce(){
    force=original_force;
}

 void
t_particle::draw(){
    //sizePx=(height*particle_radius/(1.f+1.f)*2.f);
    
    glVertex2f(position[0] , position[1]);
}

 void
t_particle::addForce(PVector external_force){
    force=force+external_force;
}

//Verlet integration
 void
t_particle::update(){
    
    auto prevPosition=position;
    
    velocity=velocity+dt/mass*force;
    position=position+velocity*dt;
    
    if (position[0]>right_ ) {
        velocity[0]=-damp*fabs(velocity[0]);
        
    } else if (position[0]<left_ ) {
        velocity[0]=damp*fabs(velocity[0]);
    }
    
    if (position[1]>floor_ -epsilon) {
        position[1]=floor_;
        velocity[1]=-damp*fabs(velocity[1]);
    } else if (position[1]<ceil_ ) {
        velocity[1]=+damp*fabs(velocity[1]);
    }
    
    //friction
    if(prevPosition[1]>floor_-epsilon&& position[1]>floor_-epsilon){
        velocity[0]=friction* velocity[0];
    }
    
}

 bool
t_particle::detectCollide(t_particle &particle){
    float dist=(position-particle.position).norm();
    if(dist<radius+particle.radius){
        return true;
    }
    
    return false;
}

 void
t_particle::respondCollide(t_particle &particle){
    PVector diff=position-particle.position;
    PVector k(diff/diff.norm());
    float a=2*k.dot(velocity-particle.velocity )/(1/mass+1/particle.mass);
    velocity=collisionDamp*(velocity-(a/mass)*k);
    particle.velocity=collisionDamp*(particle.velocity+(a/particle.mass)*k);
}

//This function handles both detect and response to collision.
 bool
t_particle::collide(t_particle &particle){
    
    bool isColliding=detectCollide(particle);
    if(isColliding){
        respondCollide(particle);
    }
    
    return isColliding;
}
