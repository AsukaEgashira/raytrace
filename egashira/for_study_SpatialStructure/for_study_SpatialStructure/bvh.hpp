//
//  bvh.hpp
//  for_study_SpatialStructure
//
//  Created by asuka egashira on 2017/08/25.
//  Copyright © 2017年 asuka egashira. All rights reserved.
//

#ifndef bvh_hpp
#define bvh_hpp

#include <vector>
using std::vector;

class bvh{
public:
    //bvh();
    bvh(vector<float> aabb_min, vector<float> aabb_max, int child_l, int child_r, int num);
    ~bvh();

    void show_bvh();
    void devideSpatial();
    
private:
    vector<float> aabb_min;
    vector<float> aabb_max;
    int child_l;
    int child_r;
    int num;
    
    //pointer of object
};

#endif /* bvh_hpp */
