//
//  global_variables.hpp
//  for_study_SpatialStructure
//
//  Created by asuka egashira on 2017/08/30.
//  Copyright © 2017年 asuka egashira. All rights reserved.
//

#ifndef global_variables_hpp
#define global_variables_hpp

#include <stdio.h>
//#include <GLFW/glfw3.h>

extern float dt;

//damp coef for colliding the wall.
extern const float damp;//0.98f;
extern const float collisionDamp;

//coef for friction at the floor.
extern const float friction;

extern const float ceil_,floor_;
extern const float left_,right_;

//max and min velocity
extern const float v_min,v_max;

//very small number.
extern const float epsilon;

extern const int num_particle;
extern const bool uniform_density;
extern const bool uniform_radius;
extern const float particle_radius;

extern const float gravity_coefficient;

#endif /* glabol_variables_hpp */
