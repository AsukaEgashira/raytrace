//
//  t_particle.hpp
//  for_study_SpatialStructure
//
//  Created by asuka egashira on 2017/08/29.
//  Copyright © 2017年 asuka egashira. All rights reserved.
//

#ifndef t_particle_hpp
#define t_particle_hpp

#include <GLFW/glfw3.h>
#include <cmath>
#include <vector>
#include "global_variables.hpp"
//#include "Node.hpp"

using std::vector;

struct PVector{
    //We have not written code to free memory.
    
    GLfloat pvec[2];
    
    inline PVector(const GLfloat x, const GLfloat y){
        pvec[0]=x;
        pvec[1]=y;
    }
    
    inline PVector(const GLfloat scalar){
        pvec[0]=scalar;
        pvec[1]=scalar;
    }
    
    //PVector(){}
    
    inline GLfloat& operator[](const int index){
        return pvec[index];
    }
    inline PVector operator*(const GLfloat scalar)const {
        return PVector(scalar*pvec[0],scalar*pvec[1]);
    }
    
    inline PVector operator/(const GLfloat scalar)const {
        return PVector(1.f/scalar*pvec[0],1.f/scalar*pvec[1]);
    }
    
    inline PVector operator+(const PVector  anotherVector) const{
        return PVector(this->pvec[0]+anotherVector.pvec[0],this->pvec[1]+anotherVector.pvec[1]);
    }
    
    inline PVector operator-(void) const{
        return PVector(-pvec[0],-pvec[1]);
    }
    
    inline PVector operator-(const PVector&  anotherVector)const{
        return *this+(-anotherVector);
    }
    
    inline void operator+=(const PVector & anotherVector){
        pvec[0]+=anotherVector.pvec[0];
        pvec[1]+=anotherVector.pvec[1];
        return;
    }
    
    
    inline void operator-=(const PVector & anotherVector){
        pvec[0]-=anotherVector.pvec[0];
        pvec[1]-=anotherVector.pvec[1];
        return;
    }
    
    inline void operator*=(const GLfloat scalar){
        pvec[0]*=scalar;
        pvec[1]*=scalar;
        return;
    }
    
    inline bool operator!=(const PVector & anotherVector){
        if(fabs(pvec[0] - anotherVector.pvec[0]) > 0.000001 && fabs(pvec[1] - anotherVector.pvec[1]) > 0.000001){
            return true;
        }
        return false;
    }
    
    inline float dot(const PVector anotherVector)const{
        return pvec[0]*anotherVector.pvec[0]+pvec[1]*anotherVector.pvec[1];
        
    }
    
    inline GLfloat norm(){
        return sqrtf(pvec[0]*pvec[0]+pvec[1]*pvec[1]);
    }
    
    ~PVector(){
    }
};

inline PVector operator*(const GLfloat scalar, PVector aVector){
    return aVector*scalar;
}


const PVector gravity=PVector(0, gravity_coefficient);


class t_particle {
public:
    PVector position;
    PVector velocity;
    PVector force;
    PVector original_force;
    float radius;
    float mass;
    float duration;//duration of existance. Not used now.
    
    int depth;
    
    t_particle(PVector position, PVector velocity, float mass,float radius/*=particle_radius*/);
    ~t_particle();
    
    void resetForce();
    
    void draw();
    
    void addForce(PVector external_force);
    
    //Verlet integration
    void update();
    
    bool detectCollide(t_particle &particle);
    
    void respondCollide(t_particle &particle);
    
    //This function handles both detect and response to collision.
    bool collide(t_particle &particle);
    
    vector<t_particle*> candidate;
    //vector<int*> subject;
    
};

#endif /* t_particle_hpp */
