//
//  glabal_variables.cpp
//  for_study_SpatialStructure
//
//  Created by asuka egashira on 2017/08/30.
//  Copyright © 2017年 asuka egashira. All rights reserved.
//

#include "global_variables.hpp"
float dt = 0.1;

//damp coef for colliding the wall.
const float damp=1.f;//0.98f;

const float collisionDamp=1.f;//0.95f;
//coef for friction at the floor.
const float friction=1.f;//0.98f;

const float ceil_=-0.9f,floor_=0.9f;
const float left_=-0.9f,right_=0.9f;

//max and min velocity
const float v_min=-1.f,v_max=1.f;

//very small number.
const float epsilon=0.001f;

const int num_particle=100;
const bool uniform_density=0;
const bool uniform_radius=0;
const float particle_radius=0.03f;

const float gravity_coefficient=0.98f;
