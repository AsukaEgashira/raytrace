//
//  QuaternarySpatial.hpp
//  for_study_SpatialStructure
//
//  Created by asuka egashira on 2017/08/25.
//  Copyright © 2017年 asuka egashira. All rights reserved.
//

#ifndef Node_hpp
#define Node_hpp

#include <stdio.h>
#include <vector>
#include "t_particle.hpp"

using std::vector;

class Node{
public:
    Node(float ceiling, float floor, float left, float right, int depth);
    ~Node();
    
    float ceiling;
    float floor;
    float left;
    float right;
    int depth;
    vector<t_particle*> belong;
    //vector<int> sub_par; //pointer of particle...
    
    vector<t_particle*> candidate;

    Node* children[4];
    Node* parent;
};

#endif
