//
//  QuaternarySpatial.cpp
//  for_study_SpatialStructure
//
//  Created by asuka egashira on 2017/08/25.
//  Copyright © 2017年 asuka egashira. All rights reserved.
//

#include "Node.hpp"

Node::Node(float _ceil, float _floor, float _left, float _right, int _depth):ceiling(_ceil), floor(_floor), left(_left), right(_right), depth(_depth){
}

Node::~Node(){

}

